package LinkedList;

class NodeLLGroups {
	int data;
	NodeLLGroups next;
	
	NodeLLGroups(int data) {
		this.data=data;
		this.next=null;
	}
}

public class ReverseLLInGroups {
	
	static NodeLLGroups reverseInGroups(NodeLLGroups head, int k) {
		if(k<=0) {
			return null;
		}
		
		NodeLLGroups current=head;
		NodeLLGroups prev=null;
		NodeLLGroups next=null;
		int count=0;
		
		while(current!=null && count<k) {
			count++;
			next=current.next;
			current.next=prev;
			prev=current;
			current=next;			
		}
		
		System.out.println("Head's data: "+head.data);
		System.out.println("Prev's data: "+prev.data);
		System.out.println("Current's data: "+current.data);
		System.out.println("Next's data: "+next.data);

		if(next!=null) {
			head.next=next;
		}
		return prev;		
	}
	
	static void printList(NodeLLGroups head) {
		NodeLLGroups current=head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}

	public static void main(String args[]) {
		NodeLLGroups head=new NodeLLGroups(1);
		head.next=new NodeLLGroups(2);
		head.next.next=new NodeLLGroups(3);
		head.next.next.next=new NodeLLGroups(4);
		head.next.next.next.next=new NodeLLGroups(5);
		head.next.next.next.next.next=new NodeLLGroups(6);
		head.next.next.next.next.next.next=new NodeLLGroups(7);
		
		System.out.print("Original List: ");
		printList(head);
		System.out.println();
		head=reverseInGroups(head,5);
		System.out.print("Reversed List: ");
		printList(head);
	}
}
