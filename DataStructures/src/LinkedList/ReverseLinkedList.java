package LinkedList;

class NodeLL {
	int data;
	NodeLL next;
	
	NodeLL(int data) {
		this.data=data;
		this.next=null;
	}
}

public class ReverseLinkedList {
	
	static NodeLL reverse(NodeLL head) {
		NodeLL current=head;
		NodeLL next=null;
		NodeLL prev=null;
		
		while(current!=null) {
			next=current.next;
			current.next=prev;
			prev=current;
			current=next;
		}
		head=prev;
		
		return head;
	}
	
	static void printList(NodeLL head) {
		NodeLL current=head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}
	
	public static void main(String args[]) {
		NodeLL head=new NodeLL(1);
		head.next=new NodeLL(2);
		head.next.next=new NodeLL(3);
		head.next.next.next=new NodeLL(4);
		head.next.next.next.next=new NodeLL(5);
		
		System.out.print("Original List: ");
		printList(head);		
		head=reverse(head);
		System.out.println();
		System.out.print("Reversed List: ");
		printList(head);
	}
}
