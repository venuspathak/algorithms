package StacksAndQueues;

import java.util.Stack;

public class StackGetMin {

	static Stack<Integer> s=new Stack<Integer>();
	static int min;
	
	static void popFromStack() {
		if(s.isEmpty())
			System.out.println("Stack is empty");
		else {
			int k=s.pop();
			
			if(k>min) {
				System.out.println("Popped element is: "+k);
				System.out.println("Min is: "+min);
			}
			else {
				min=2*min-k;
				System.out.println("Popped element is: "+k);
				System.out.println("Min is: "+min);
			}
		}		
	}
	
	static void pushIntoStack(int k) {
		if(s.isEmpty()) {
			s.push(k);
			min=k;
		}
		else {
			if(k>min) {
				// Push element into stack
			s.push(k);
			}
			else {
				// Push updated element into stack
				s.push(2*k-min);
				// Update min
				min=k;
			}
		}
	}
	
	static void getMin() {
		if(s.isEmpty())
			System.out.println("Stack is empty");
		else {
			System.out.println("Min is "+min);
		}
	}
	
	static void printStack() {
		if(s.isEmpty()) {
			return;
		}		
		
		int k=s.pop();			
		printStack();			
		System.out.print(k+" ");			
		s.push(k);				
	}	
	
	public static void main(String args[]) {
		System.out.println("START PUSHING");
		pushIntoStack(3);
		pushIntoStack(5);
		pushIntoStack(2);
		pushIntoStack(1);
		pushIntoStack(1);
		pushIntoStack(-1);
		
		printStack();
		
		System.out.println("GET MIN");		
		getMin();
		
		System.out.println("START POPPING");	
		popFromStack();
		popFromStack();
		popFromStack();
		popFromStack();
		popFromStack();
		popFromStack();
		popFromStack();
	}
}
