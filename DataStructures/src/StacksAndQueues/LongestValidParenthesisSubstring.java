package StacksAndQueues;

import java.util.Stack;

public class LongestValidParenthesisSubstring {
	
	static int findLongest(String str) {
		int length=0;
		Stack<Character> stack = new Stack<Character>();
		
		for(int i=0; i<str.length(); i++) {
			Character currentChar=str.charAt(i);
			
			if(currentChar=='(')
				stack.push(currentChar);
			else if(currentChar==')') {
				if(stack.isEmpty()) {
					System.out.println("Character "+currentChar+" is discarded");
				}
				else {
					stack.pop();
					length=length+2;
				}
			}
		}		
		return length;
	}
	
	public static void main(String args[]) {
		String str="()(()))))";
		
		int result=findLongest(str);
		if(result==-1) {
			System.out.println("There is no valid substring");
		}
		else {
			System.out.println("Longest valid substring is of size: "+result);
		}
	}
}
