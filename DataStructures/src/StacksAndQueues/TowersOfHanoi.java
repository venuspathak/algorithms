package StacksAndQueues;

public class TowersOfHanoi {

	static void moveDisks(int n, char fromDisk, char toDisk, char auxDisk) {
		// Move disks from fromDisk to toDisk using auxDisk
		
		if(n==1) {
			System.out.println("Disk 1 moved from "+fromDisk+" to "+toDisk);
			return;	
		}
		// Move (n-1) disks from fromDisk to auxDisk
		moveDisks(n-1,fromDisk,auxDisk,toDisk);
		// Move nth disk from fromDisk to toDisk
		System.out.println("Disk "+n+" moved from "+fromDisk+" to "+toDisk);
		// Move (n-1) disks from auxDisk to toDisk
		moveDisks(n-1,auxDisk,toDisk,fromDisk);
	}
	
	public static void main(String args[]) {
		int n=3;
		moveDisks(n,'A','C','B');
	}
}
