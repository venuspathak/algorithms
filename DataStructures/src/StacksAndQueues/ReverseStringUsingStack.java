package StacksAndQueues;

import java.util.Stack;

public class ReverseStringUsingStack {
	
	static String reverse(String str, Stack<Character> s) {
		if (str.isEmpty()) {
			return null;
		}
		for(int i=0; i<str.length(); i++) {
			s.push(str.charAt(i));
		}
		
		String reverse="";
		while(!s.isEmpty()) {
			reverse=reverse+s.pop();
		}
		return reverse;
	}
	
	public static void main(String args[]) {
		String str="GeeksQuiz";
		Stack<Character> s = new Stack<Character>();
		
		System.out.println("String before reversal is: "+str);
		String reversed=reverse(str, s);
		System.out.println("String after reversal is: "+reversed);
	}
}
