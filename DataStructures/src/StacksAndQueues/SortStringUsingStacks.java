package StacksAndQueues;

import java.util.Stack;
import java.util.Arrays;

public class SortStringUsingStacks {
	
	static int[] sort(int[] arr, Stack<Integer> s) {
		Stack<Integer> tempStack=new Stack<Integer>();
		Object[] returnArr;
		
		for(int i=0; i<arr.length; i++) {
			//System.out.println("Element: "+arr[i]);
			if(s.isEmpty()) {
				s.push(arr[i]);
			} else {
				if(arr[i]<s.peek()) {
					// Push the element
					s.push(arr[i]);
				}
				else {
					while(!s.isEmpty() && arr[i]>s.peek()) {
						tempStack.push(s.pop());
					}
					s.push(arr[i]);
					while(!tempStack.isEmpty()) {
						s.push(tempStack.pop());
					}
				}
			}
			
			//System.out.print("Stack currently is: ");
			//System.out.println(Arrays.toString(s.toArray()));
		}
		
		// Pop from stack and add to array
		int i=0;
		while(!s.isEmpty()) {
			arr[i]=s.pop();
			i++;
		}
		return arr;
	}
	
	public static void main(String args[]) {
	
		Stack<Integer> s = new Stack<Integer>();
		int[] arr = new int[]{8,5,7,1,9,12,10};
		System.out.println("Array before sort: "+Arrays.toString(arr));
		int[] sortedArr = sort(arr,s);
		System.out.println("Array after sort: "+Arrays.toString(sortedArr));
	}
}
