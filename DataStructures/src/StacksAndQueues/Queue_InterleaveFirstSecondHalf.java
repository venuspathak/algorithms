package StacksAndQueues;

import java.util.Queue;
import java.util.LinkedList;
import java.util.Stack;

public class Queue_InterleaveFirstSecondHalf {
	static Queue<Integer> q = new LinkedList<Integer>();
	static Stack<Integer> s = new Stack<Integer>();
	
	static void interleave() {		
		// Return if the size of queue is not even
		if(!(q.size()%2==0))
			return;
		
		// Calculate first half size
		int half=q.size()/2;
		
		// Put first half into stack
		// Stack - 2(T),1
		// Queue - 3,4
		for(int i=0; i<half; i++){
			s.push(q.poll());
		}
		
		// Pop elements from stack and add to queue
		// Queue - 3,4,2,1
		while(!s.isEmpty()) {
			q.add(s.pop());
		}
		
		// Poll first half and add to queue
		// Queue - 2,1,3,4
		for(int i=0; i<half; i++){
			q.add(q.poll());
		}
		
		// Put first half into stack
		// Stack - 1(T),2
		// Queue - 3,4
		for(int i=0; i<half; i++){
			s.push(q.poll());
		}
		
		// Do the following until stack becomes empty:
		// Pop an element from stack and then put into queue. Then poll an element from queue and add to queue
		// Queue - 4,1,3
		// Queue - 1,3,2,4
		while(!s.isEmpty()) {
			q.add(s.pop());
			q.add(q.poll());
		}
	}
	
	public static void main(String args[]) {
		q.add(1);
		q.add(2);
		q.add(3);
		q.add(4);
		q.add(5);
		q.add(6);
		
		System.out.println("Queue before interleaving: "+q);		
		interleave();
		System.out.println("Queue after interleaving: "+q);
	}
}
