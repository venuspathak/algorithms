package StacksAndQueues;

import java.util.Stack;

public class Postfix_To_Infix {

	static boolean isOperator(char x) {
		switch(x) {
		case '+':
			System.out.println("Character "+x+" is an addition operator");
			return true;
		case '-':
			System.out.println("Character "+x+" is a subtraction operator");
			return true;		
		case '*':
			System.out.println("Character "+x+" is a multiplication operator");
			return true;
		case '/':
			System.out.println("Character "+x+" is a division operator");
			return true;
		}
		System.out.println("Character "+x+" is not an operator");
		return false;
	}
	
	static String convert(String postfix) {	
		Stack<String> s = new Stack<String>();
		String expression="";
		
		for(int i=0; i<postfix.length(); i++) {
			char currentChar = postfix.charAt(i);
			if(isOperator(currentChar)) {
				if(!s.isEmpty()) {
					// Character is an operator
					String first=s.pop();
					String second=s.pop();
					expression=first+currentChar+second;
					s.push(expression);
				}
			}
			else {
				// Character is not an operator
				String currentString=Character.toString(currentChar);
				s.push(currentString);
			}
		}
		expression=new StringBuilder(expression).reverse().toString();
		return expression;
	}
	
	public static void main(String args[]) {
		String postfix = "ABC*+";
		String infix=convert(postfix);
		System.out.println("Prefix expression is: "+postfix);
		System.out.println("Infix expression is: "+infix);
	}
}
