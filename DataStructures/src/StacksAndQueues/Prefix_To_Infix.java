package StacksAndQueues;

import java.util.Stack;

public class Prefix_To_Infix {

	static boolean isOperator(char x) {
		switch(x) {
		case '+':
			System.out.println("Character "+x+" is an addition operator");
			return true;
		case '-':
			System.out.println("Character "+x+" is a subtraction operator");
			return true;		
		case '*':
			System.out.println("Character "+x+" is a multiplication operator");
			return true;
		case '/':
			System.out.println("Character "+x+" is a division operator");
			return true;
		}
		System.out.println("Character "+x+" is not an operator");
		return false;
	}
	
	static String convert(String prefix) {
		prefix=new StringBuilder(prefix).reverse().toString();
		System.out.println("String after reversal: "+prefix);
		
		Stack<String> s = new Stack<String>();
		String expression="";
		
		for(int i=0; i<prefix.length(); i++) {
			char currentChar = prefix.charAt(i);
			if(isOperator(currentChar)) {
				if(!s.isEmpty()) {
					// Character is an operator
					String first=s.pop();
					String second=s.pop();
					expression=first+currentChar+second;
					s.push(expression);
				}
			}
			else {
				// Character is not an operator
				String currentString=Character.toString(currentChar);
				s.push(currentString);
			}
		}
		return expression;
	}
	
	public static void main(String args[]) {
		String prefix = "+A*BC";
		String infix=convert(prefix);
		System.out.println("Prefix expression is: "+prefix);
		System.out.println("Infix expression is: "+infix);
	}
}
