package StacksAndQueues;

import java.util.Stack;

public class ReverseStackUsingRecursion {
	
	static void insertAtBottom(Stack<Integer> s, int poppedElement) {
		if(s.isEmpty()) {
			s.push(poppedElement);			
		}
		else {
			int x=s.pop();
			insertAtBottom(s,poppedElement);			
			s.push(x);		
		}		
	}
	
	static void sort(Stack<Integer> s) {
		if(s.isEmpty()) {
			return;
		}
		else {
			int poppedElement=s.pop();
			sort(s);
			
			// Insert at bottom
			insertAtBottom(s,poppedElement);
		}		
	}
	
	public static void main(String args[]) {
		Stack<Integer> s = new Stack<Integer>();
		s.push(0);
		s.push(3);
		s.push(2);
		s.push(1);		
		
		System.out.println("Stack before reversal: "+s);		
		sort(s);
		System.out.println("Stack after reversal: "+s);
	}	
}
