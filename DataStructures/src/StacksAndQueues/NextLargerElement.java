package StacksAndQueues;

import java.util.Stack;

public class NextLargerElement {
	
	static void printLargerElement(int[] arr) {
		Stack<Integer> s=new Stack<Integer>();
		
		// Push the first element
		s.push(arr[0]);
		
		for(int i=1; i<arr.length; i++) {
			int next=arr[i];
			
			if(next>s.peek()) {
				while(!s.isEmpty() && next>=s.peek()) {
					s.pop();
					System.out.print(next+" ");
				}
				s.push(next);
			}
			else {
				s.push(next);
			}			
		}
		
		while(!s.isEmpty())
		{
			s.pop();
			System.out.print(-1+" ");
		}

	}
	
	public static void main(String args[]) {
		int[] arr=new int[] {4,3,2,1};
		
		printLargerElement(arr);
	}
}
