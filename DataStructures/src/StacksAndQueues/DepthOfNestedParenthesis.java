package StacksAndQueues;

import java.util.Stack;

public class DepthOfNestedParenthesis {
	
	static int findDepth(String str) {
		int depth=0;
		Stack<Character> stack=new Stack<Character>();
		
		for(int i=0; i<str.length(); i++) {
			char currentChar=str.charAt(i);
			if(currentChar=='(') {
				stack.push(currentChar);
				if(stack.size()>depth)
					depth=stack.size();
			}
			else if(currentChar==')') {
				stack.pop();
				if(stack.size()>depth)
					depth=stack.size();
			}
		}
		return depth;		
	}
	
	public static void main(String args[]) {
		String str="( p((q)) ((s)t) )";
		
		System.out.println("Depth of parenthesis: "+findDepth(str));
	}
}
