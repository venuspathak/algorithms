package StacksAndQueues;

import java.util.Stack;

public class CelebrityProblem {

	static int[][] matrix = new int[][] {
		{0,0,0,1},
		{0,0,0,1},
		{0,0,0,1},
		{0,0,0,0}
	};
	
	static boolean knows(int a, int b) {
		boolean returnVal=(matrix[a][b]==1)?true:false;
		return returnVal;
	}
	
	static int findCelebrity(int n) {
		int c;
		
		// Push all elements into a stack
		Stack<Integer> st = new Stack<Integer>();
		
		for(int i=0; i<n; i++){
			st.push(i);
		}
		
		while(st.size()>1) {
			int a=st.pop();
			int b=st.pop();
			
			if(knows(a,b)) {
				st.push(b);
			}
			else {
				st.push(a);
			}
		}
		
		c=st.pop();
		
		for(int i=0; i<n; i++) {
			if(c!=i && (knows(c,i) || !knows(i,c)))
				return -1;
		}
		return c;
	}
	
	public static void main(String args[]) {
		int n=4;
		
		int result=findCelebrity(n);
		if(result==-1) {
			System.out.println("No Celebrity found");
		}
		else {
			System.out.println("Celebrity found: "+result);
		}
	}
}
