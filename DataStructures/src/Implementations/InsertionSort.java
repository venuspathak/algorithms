package Implementations;

public class InsertionSort {
	
	static void insertionSort(int[] arr) {
		for(int i=1; i<arr.length; i++) {
			int key=arr[i];
			int j=i-1;
			
			while(j>=0 && key<arr[j]) {
				arr[j+1]=arr[j];
				j=j-1;
			}
			arr[j+1]=key;
		}
	}
	
	static void printArray(int[] arr) {
		for(int i=0; i<arr.length; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
	
	public static void main(String args[]) {
		int arr[] = new int[]{2,12,0,3,9,1};
		
		printArray(arr);
		insertionSort(arr);
		printArray(arr);
	}
}
