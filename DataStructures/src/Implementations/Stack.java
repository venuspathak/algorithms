package Implementations;

public class Stack {
	int top;
	int MAX=1000;
	int a[]=new int[MAX];

	Stack() {
		top=-1;
	}
	
	boolean isEmpty() {
		if(top<0)
			return false;
		else 
			return true;
	}
	
	boolean push(int n) {
		//System.out.println(top);
		if(top>=(MAX-1)) {
			System.out.println("Stack Overflow");
			return false;
		}
		else {
			top=top+1;
			a[top]=n;
			return true;
		}
	}
	
	int pop() {
		if(top<0) {
			System.out.println("Stack Underflow");
			return 0;
		}
		else {
			int n=a[top];
			top=top-1;
			return n;
		}
	}
	
	void print(Stack s){
		while(top>=0) {
			int n=s.pop();
			System.out.println("Element: "+n);
		}
	}
	
	public static void main(String args[]) {
		Stack s = new Stack();
		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);
		s.push(50);
		System.out.println("Element popped is: "+s.pop());
		System.out.println("Element popped is: "+s.pop());
		s.push(40);
		
		s.print(s);
	}
}
