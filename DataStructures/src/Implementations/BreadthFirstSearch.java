package Implementations;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ListIterator;

public class BreadthFirstSearch {
	// Initialize
	int V;
	LinkedList<Integer> adj[];
	
	// Constructor
	BreadthFirstSearch(int v) {
		V=v;
		adj=new LinkedList[V];
		
		for(int i=0; i<V; i++)
			adj[i]=new LinkedList();
	}
	
	// Add to adjacency list
	void addEdge(int v, int w) {
		adj[v].add(w);
	}
	
	// Search
	void search(int s) {
		boolean visited[] = new boolean[V];
		Queue<Integer> queue = new LinkedList<Integer>();
		
		visited[s]=true;
		queue.add(s);
		
		while(queue.size()!=0) {
			s=queue.poll();
			System.out.print(s+" ");
			
			ListIterator<Integer> list=adj[s].listIterator();
			while(list.hasNext()) {
				int n=list.next();
				if(!visited[n]) {
				visited[n]=true;
				queue.add(n);
				}
			}
		}
	}
	
	// main function
	public static void main(String args[]) {
		BreadthFirstSearch bfs = new BreadthFirstSearch(4);
		bfs.addEdge(0, 1);
		bfs.addEdge(0, 2);
		bfs.addEdge(1, 2);
		bfs.addEdge(2, 0);
		bfs.addEdge(2, 3);
		bfs.addEdge(3, 3);
		
		bfs.search(2);
	}
}
