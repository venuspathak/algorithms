package Implementations;

public class MergeSort {

	public void merge(int arr[], int l, int m, int r) {
		int n1=m-l+1;
		int n2=r-m;
		
		int L[] = new int[n1];
		int K[] = new int[n2];
		
		for(int i=0; i<n1; i++)
			L[i]=arr[l+i];
		
		for(int j=0; j<n2; j++)
			K[j]=arr[m+1+j];
		
		int i=0;
		int j=0;
		int k=l;
		while(i<n1 && j<n2) {
			if(L[i]<=K[j]) {
				arr[k]=L[i];
				i++;
			}
			else {
				arr[k]=K[j];
				j++;
			}
			k++;
		}
		
		while(i<n1) {
			arr[k]=L[i];
			i++;
			k++;
		}
		
		while(j<n2) {
			arr[k]=K[j];
			j++;
			k++;
		}
	}

	public void sort(int arr[], int l, int r) {
		if(l<r) {
		int m = (l + r) / 2;
		sort(arr, l, m);
		sort(arr, m + 1, r);

		merge(arr, l, m, r);
		}
	}

	public void print(int arr[]) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	public static void main(String args[]) {
		MergeSort obj = new MergeSort();
		int arr[] = { 6, 99, 0, 1, 56, 20 };

		obj.print(arr);
		obj.sort(arr,0,arr.length-1);
		obj.print(arr);
	}
}
