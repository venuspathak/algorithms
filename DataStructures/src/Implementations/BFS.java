package Implementations;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ListIterator;

public class BFS {
	// Initialize
	int V;
	LinkedList<Integer> adj[];
	
	// Constructor
	BFS(int val) {
		V=val;
		adj=new LinkedList[V];
		
		for(int i=0; i<V; i++)
			adj[i]=new LinkedList();
	}
	
	// Add edge to adjacency list
	void addEdge(int v, int w) {
		adj[v].add(w);
	}
	
	// Search function
	void search(int s) {
		// Create a boolean array to record whether a particular vertex has been visited or not
		boolean visited[]=new boolean[V];
		
		// Create a queue for BFS
		Queue<Integer> queue = new LinkedList<Integer>();
		
		// Mark the current vertex as visited and add it to queue
		visited[s]=true;
		queue.add(s);
		
		while(queue.size()!=0) {
			// Remove the front vertex from queue
			s=queue.poll();			
			System.out.print(s+" ");
			
			ListIterator<Integer> list = adj[s].listIterator();
			while(list.hasNext()) {
				int n=list.next();
				if(!visited[n]) {
					visited[n]=true;
					queue.add(n);
				}
			}
		}
	}
	
	// main function
	public static void main(String args[]) {
		BFS bfs = new BFS(4);
		
		bfs.addEdge(0, 1);
		bfs.addEdge(0, 2);
		bfs.addEdge(1, 2);
		bfs.addEdge(2,0);
		bfs.addEdge(2,3);
		bfs.addEdge(3,3);
		
		bfs.search(2);
	}
}
