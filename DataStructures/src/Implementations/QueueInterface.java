package Implementations;
import java.util.Queue;
import java.util.LinkedList;

public class QueueInterface {

	public static void main(String args[]) {
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(1);
		q.add(2);
		q.add(3);
		q.add(4);
		
		System.out.println("Element popped: "+q.poll());
		System.out.println("Element popped: "+q.poll());
		System.out.println("Element popped: "+q.poll());
	}
}
