package Implementations;

public class BinarySearchTree {
	static Node root;
	static class Node{
		int key;
		Node left,right;
		
		Node(int val) {
			key=val;
			left=right=null;
		}
	}
	
	BinarySearchTree() {
		root=null;
	}
	
	BinarySearchTree(Node node) {
		root=node;
	}
	
	static Node insertElement(Node root, int val) {
		if(root==null) {
			root=new Node(val);
			return root;
		}
		
		if(val<root.key) {
			root.left=insertElement(root.left,val);		
		} else {
			root.right=insertElement(root.right,val);
		}
		return root;
	}
	
	static void inorder(Node node) {
		if(node==null)
			return;
		inorder(node.left);
		System.out.print(node.key+" ");
		inorder(node.right);
	}
	
	static Node search(Node root, int val) {
		if(root.key==val) {
			return root;
		}
		else if (val<root.key) {
			return search(root.left,val);
		}
		else {
			return search(root.right,val);
		}
	}
	
	public static void main(String args[]) {
		BinarySearchTree tree = new BinarySearchTree(new Node(50));
		root=insertElement(root,30);
		root=insertElement(root,20);
		root=insertElement(root,40);
		root=insertElement(root,70);
		root=insertElement(root,60);
		root=insertElement(root,80);
		
		inorder(root);
		
		Node node=search(root,20);
		System.out.println("HELLO:"+node.key);
	}
}
	
