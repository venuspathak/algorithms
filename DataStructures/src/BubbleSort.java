
public class BubbleSort {

	static void bubbleSort(int array[], int n) {
		int i, j, temp;
		boolean swapped;
		
		for(i=0; i < n-1; i++) {
			swapped = false;
			for (j=0; j < n-i-1; j++) {
				if (array[j] > array[j+1]) {
					temp = array[j];
					array[j] = array[j+1];
					array[j+1] = temp;
					swapped = true;
				}
			}
			if (swapped == false) {
				break;		
			}
		}
	}
	
	static void printArray(int array[]) {
		for (int i=0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}		
	}
	
	public static void main(String args[]) {
		int arr[] = {64, 34, 25, 12, 22, 11, 90};
		int n = arr.length;
		
		bubbleSort(arr, n);
		System.out.println("The sorted array is: ");
		printArray(arr);
	}
}
