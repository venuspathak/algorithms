
public class Fibonacci {

	static int findFibRecursion(int n) {
		if(n<=1)
			return n;
		else 
			return (findFibRecursion(n-1)+findFibRecursion(n-2));
	}
	
	static int findFibIteration(int n) {
		if(n<=1)
			return n;
		else {
			int prevPrev;
			int prev=1;
			int current=1;
			for(int i=2; i<n; i++) {
				prevPrev=prev;
				prev=current;
				current=prev+prevPrev;
			}
			return current;
		}
		
	}
	
	public static void main(String args[]) {
		int n=3;
		
		System.out.println(findFibRecursion(n));
		System.out.println(findFibIteration(n));
	}
}
