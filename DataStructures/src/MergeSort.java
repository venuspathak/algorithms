
public class MergeSort {
	
	// Function Merge
    // Merges two sub-arrays of array[].
    // First sub-array is array[s..m]
    // Second sub-array is array[m+1..e]
	void merge(int array[], int s, int m, int e) {
		// Sizes of two sub-arrays to be merged
		int size1 = (m - s) + 1;
		int size2 = e - m;
		
		// Create two temp sub-arrays with sizes size1 and size2
		int subArray1[] = new int[size1];
		int subArray2[] = new int[size2];
		
		// Copy data to temp arrays		
		for(int i = 0; i < size1; i++)
			subArray1[i] = array[s + i];
		for(int j = 0; j < size2; j++)
			subArray2[j] = array[m + 1 + j];
		
		// Merge the temp arrays
		int i = 0, j = 0;
		int k = s;
		
		while(i < size1 && j < size2) {
			if (subArray1[i] <= subArray2[j]) {
				array[k] = subArray1[i];
				i++;
			}
			else {
				array[k] = subArray2[j];
				j++;
			}		
			k++;
		}
		
		while(i < size1) {
			array[k] = subArray1[i];
			i++;
			k++;			
		}
		
		while(j < size2) {
			array[k] = subArray2[j];
			j++;
			k++;
		}
	}
	
	// Function Sort
	void sort(int array[], int start, int end) {
		
		if(start < end) {
		int mid = (start + end)/2;
		
		sort(array, start, mid);
		sort(array, mid+1, end);
		merge(array, start, mid, end);
		}
	}
	
	// Function Main
	public static void main(String args[]) {
		MergeSort obj = new MergeSort();
		
		int arr[] = {12, 11, 13, 5, 6, 7};
		int len = arr.length;
		
		obj.sort(arr, 0, len - 1);

		System.out.println("The sorted array is: ");
		for(int i = 0; i < arr.length; i++)
			System.out.print(arr[i] + " ");
	}
}
