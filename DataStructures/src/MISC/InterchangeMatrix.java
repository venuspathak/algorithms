package MISC;

public class InterchangeMatrix {
	
	static int[][] interchange(int[][] matrix,int i,int j) {
		for(int row=0; row<i; row++) {
			int temp=matrix[row][0];
			matrix[row][0]=matrix[row][j-1];
			matrix[row][j-1]=temp;			
		}
		
		return matrix;
	}

	public static void main(String args[]) {
		int i=4;
		int j=4;
		int[][] matrix={
				{1,2,3,4},
				{5,6,7,8},
				{9,0,1,2},
				{3,4,5,6}
		};
		
		System.out.println("Before interchange: ");
		for(int row=0; row<i; row++) {
			for(int col=0; col<j; col++) {
				System.out.print(matrix[row][col]+" ");
			}
			System.out.println();
		}
		
		matrix=interchange(matrix,i,j);
		
		System.out.println("After interchange: ");
		for(int row=0; row<i; row++) {
			for(int col=0; col<j; col++) {
				System.out.print(matrix[row][col]+" ");
			}
			System.out.println();
		}
	}
}
