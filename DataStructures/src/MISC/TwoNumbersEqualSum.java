package MISC;
import java.util.HashSet;

public class TwoNumbersEqualSum {	
	
	static boolean binarySearch(int[] arr, int elementToSearch, int low, int high) {	
		int mid=(high+low)/2;
		
		while(low<=high) {
			if(arr[mid]==elementToSearch)
				return true;		
			else if(elementToSearch<arr[mid])
				return binarySearch(arr,elementToSearch,low,mid-1);
			else
				return binarySearch(arr,elementToSearch,mid+1,high);
		}
		
		return false;
	}
	
	static boolean hasPair(int[] arr, int sum, int length) {
		for(int i=0; i<length; i++) {
			int elementToSearch=sum-arr[i];
			if(binarySearch(arr,elementToSearch,i+1,length-1)){
				System.out.println("Pair is "+arr[i]+","+elementToSearch);
				return true;
			}
		}
		return false;
	}
	
	static boolean optimizedHasPair(int[] arr, int sum, int length) {
		int i=0;
		int j=(length-i-1);
			
		
		while(i<j) {
			int sumIAndJ=arr[i]+arr[j];

			if(sumIAndJ>sum) {
				// Sum of arr[i] and arr[j] is larger than sum. Decrement j
				j--;
			}
			else if(sumIAndJ<sum) {
				i++;
			}
			else if(sumIAndJ==sum) {
				System.out.println("Pair is: "+arr[i]+","+arr[j]);
				return true;
			}
		}
		
		return false;		
	}
	
	static boolean optimizedWithoutSorted(int[] arr, int sum, int length) {
		HashSet<Integer> hs=new HashSet<Integer>();
		
		for(int i=0; i<length;i++) {
			int complement=sum-arr[i];
			
			if(!hs.contains(complement)) {
				hs.add(complement);
			} else {
				System.out.println("Pair is: "+arr[i]+","+complement);
				return true;
			}					
		}
		return false;
	}
		
	public static void main(String args[]) {
		int arr[]=new int[]{1,2,4,4};
		int sum=8;
		int length=arr.length;
		
		long startRegular=System.nanoTime();
		if(!hasPair(arr,sum,length)) {
			System.out.println("Pair is not found");
		}
		else {
			System.out.println("Pair is found");
		}
		long stopRegular=System.nanoTime();
		long diffRegular=stopRegular-startRegular;
		System.out.println("Time taken by regular solution: "+(diffRegular/1000000));
		
		long startOptimized=System.nanoTime();
		if(!optimizedHasPair(arr,sum,length)) {
			System.out.println("Pair is not found");
		}
		else {
			System.out.println("Pair is found");
		}
		long stopOptimized=System.nanoTime();
		long diffOptimized=stopOptimized-startOptimized;
		System.out.println("Time taken by optimized solution: "+(diffOptimized/1000000));
		
		if(!optimizedWithoutSorted(arr,sum,length)) {
 			System.out.println("Pair is not found");
		}
		else {
 			System.out.println("Pair is found");
		}
	}
}
