package MISC;

public class NumberDivisibleBySum {

	static boolean check(int n) {
		String str=Integer.toString(n);
		int sum=0;
		
		for(int i=0; i<str.length(); i++) {
			Character ch=str.charAt(i);
			sum=sum+Character.getNumericValue(ch);
		}
		System.out.println("Sum is: "+sum);
		
		if(n%sum==0)
			return true;
		else
			return false;
	}
	
	public static void main(String args[]) {
		int n=123;
		
		boolean result=check(n);
		if(result) {
			System.out.println("Number is divisible by sum of its digits");
		}
		else
			System.out.println("Number is not divisible by sum of its digits");
	}
}
