package MISC;

import java.util.ArrayList;
import java.util.List;

public class DistinctStringPermutations {

	static void distinctPermutations(String str, int start, int end) {
		List<String> list = new ArrayList<String>();
		
		if(start==end) {
			if(!list.contains(str)) {
				list.add(str);
				System.out.println(str);
			}
		} else {
			for(int i=start; i<=end; i++) {
				str=swap(str,start,i);
				distinctPermutations(str,start+1,end);
				str=swap(str,start,i);
			}
		}		
	}
	
	static String swap(String str, int i, int j) {
		char[] arr = str.toCharArray();
		char temp=arr[i];
		arr[i]=arr[j];
		arr[j]=temp;
		return String.valueOf(arr);
	}
	
	public static void main(String args[]) {
		String str="ABC";
		int len=str.length();
		
		distinctPermutations(str,0,len-1);
	}
}
