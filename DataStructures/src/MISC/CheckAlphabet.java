package MISC;

public class CheckAlphabet {

	static void check(Character ch) {
		if(ch>='a' && ch<='z')
			System.out.println("Character is a lowercase alphabet");
		else if (ch>='A' && ch<='Z')
			System.out.println("Character is an uppercase alphabet");
		else
			System.out.println("Character is not an alphabet");
	}
	
	public static void main(String args[]) {
		Character ch1 = 'a';
		Character ch2 = 'F';
		Character ch3 = '7';
		
		check(ch1);
		check(ch2);
		check(ch3);
	}
}
