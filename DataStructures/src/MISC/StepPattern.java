package MISC;

public class StepPattern {
	static int min=1;
	static int max=3;
	static boolean trigger=false;
	
	static void printPattern(String str) {
		int increment=1;
		
		for(int i=1; i<=str.length(); i++) {
			//System.out.println(increment);
			
			StringBuilder sb=new StringBuilder(increment);
			for(int j=0; j<increment-1; j++) {
				sb=sb.append("*");
			}
			sb=sb.append(str.charAt(i-1));
			System.out.println(sb.toString());

			if(increment==max)
				trigger=true;
			else if(increment==min)
				trigger=false;
			
			if(trigger==true)
				increment--;
			else if(trigger==false)
				increment++;
		}
	}
	
	public static void main(String args[]) {
		String str="abcdefghijk";
		printPattern(str);	
	}
}
