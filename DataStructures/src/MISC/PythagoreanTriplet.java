package MISC;

import java.util.Arrays;

public class PythagoreanTriplet {
	
	static void findTriplet(int[] arr) {
		int len = arr.length;
		
		int sq[] = new int[len];
		for(int i=0; i<len; i++) {
			sq[i]=arr[i]*arr[i];
		}
		
		Arrays.sort(sq);
		
		for(int i=len-1; i>0; i--) {
			int addition=sq[i];
			int firstIndex=0;
			int secondIndex=i-1;			
			
			while(firstIndex<secondIndex) {
				int firstElement=sq[firstIndex];
				int secondElement=sq[secondIndex];

				if(firstElement+secondElement<addition)
					firstIndex++;
				else if(firstElement+secondElement>addition) {
					secondIndex--;
				}
				else {
					System.out.println("Pythagorean triplets are: "+(int)Math.sqrt(firstElement)+","+(int)Math.sqrt(secondElement)+","+(int)Math.sqrt(addition));
					return;
				}
			}
		}

	}
	
	public static void main(String args[]) {
		int arr[] = {3, 1, 4, 6, 5};
		
		findTriplet(arr);
	}
}
