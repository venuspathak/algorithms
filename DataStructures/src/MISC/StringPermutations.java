package MISC;

public class StringPermutations {

	static void permutations(String str, int start, int end) {
		
		if(start==end) {
			System.out.println(str);
		}
		else {			
			for(int i=start; i<=end; i++) {			
				str=swap(str,start,i);
				permutations(str,start+1,end);
				str=swap(str,start,i);
			}
		}
	}
	
	static String swap(String str, int i, int j) {
		char[] arr=str.toCharArray();
		char temp=arr[i];
		arr[i]=arr[j];
		arr[j]=temp;
		return String.valueOf(arr);
	}
	
	public static void main(String args[]) {
		String str="ABC";
		int len=str.length();
		permutations(str,0,len-1);
	}
}
