package MISC;

public class ReplaceArrayElements {

	static void printArray(int[] arr) {
		for(int i=0; i<arr.length; i++)
			System.out.print(arr[i]+" ");
		System.out.println();
	}
	
	static void replace(int[] arr) {
		int len=arr.length;
		int first=arr[0];
		int second=arr[1];
		
		for(int i=0; i<len; i++) {
			if(i==(len-2))
				arr[i]=arr[len-1]+first;
			else if(i==(len-1))
				arr[i]=first+second;
			else
				arr[i]=arr[i+1]+arr[i+2];
		}
		
	}
	public static void main(String args[]) {
		int arr[] = new int[]{3,4,2,1,6};
		printArray(arr);
		replace(arr);
		printArray(arr);
	}
}
