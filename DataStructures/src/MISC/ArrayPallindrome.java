package MISC;

public class ArrayPallindrome {

	static boolean check(int[] arr) {
		boolean result=false;
		int len=arr.length;
		int mid=(0+len)/2;
		
		for(int i=0; i<mid;i++) {
			int j=len-(i+1);
			System.out.println("arr[i] is: "+arr[i]);
			System.out.println("arr[j] is: "+arr[j]);
			if(arr[i]==arr[j]) {
				result=true;
			}					
			else {
				result=false;
				break;
			}
		}
		return result;
	}
	public static void main(String args[]) {
		int[] arr = new int[]{3,6,0,6,3};
		
		boolean result=check(arr);
		
		if(result)
			System.out.println("Array is pallindrome");
		else
			System.out.println("Array is not pallindrome");
	}
}
