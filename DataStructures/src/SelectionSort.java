
public class SelectionSort {
	
	void sort(int array[]) 
	{
		int n = array.length;
		
		for(int i=0; i < n-1; i++)
		{
			int min = i;
			for(int j=i+1; j < n; j++)
			{
				if(array[j] < array[min]) 
				{
					// Update min
					min = j;
				}
			}
			// Swap array[i] and array[min]
			int temp = array[i];
			array[i] = array[min];
			array[min] = temp;
		}
	}

	// Program driver
	public static void main(String args[])
	{
		SelectionSort obj = new SelectionSort();
		int array[] = {57, 8298, 1, 84, 0};
		obj.sort(array);
		
		System.out.println("The sorted array is");
		for(int i=0; i<array.length;i++)
			System.out.print(array[i] + " ");
	}
}
