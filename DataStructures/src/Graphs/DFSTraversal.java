package Graphs;

import java.util.LinkedList;
import java.util.ListIterator;

class Graph {
	static int vertices;
	static LinkedList<Integer> adjList[];

	Graph(int vertices) {
		this.vertices=vertices;
		adjList=new LinkedList[vertices];
	
		for(int i=0; i<vertices; i++) {
			adjList[i]=new LinkedList();
		}
	}
	
	static void addEdge(int v, int w) {
		adjList[v].add(w);
	}
	
	static void traversal(int start, Boolean[] visited) {
		visited[start]=true;		
		System.out.print(start+" ");
		
		ListIterator<Integer> list=adjList[start].listIterator();
		while(list.hasNext()) {
			int n=list.next();
			
			if(!visited[n]) {
				traversal(n,visited);
			}
		}
	}
}

public class DFSTraversal {
	public static void main(String args[]) {
		int vertices=4;
		Graph g = new Graph(vertices);
		Graph.addEdge(0,1);
		Graph.addEdge(0,2);
		Graph.addEdge(1,2);
		Graph.addEdge(2,0);
		Graph.addEdge(2,3);
		Graph.addEdge(3,3);

		int start=2;
		Boolean[] visited = new Boolean[vertices];
		for(int i=0; i<vertices; i++) {
			visited[i]=false;
		}
		Graph.traversal(start, visited);
	}
}
