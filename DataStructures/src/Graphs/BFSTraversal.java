package Graphs;

import java.util.LinkedList;
import java.util.Queue;
import java.util.ListIterator;

class BFSGraph {
	static int vertices;
	static LinkedList<Integer> adjList[];

	BFSGraph(int vertices) {
		this.vertices=vertices;
		adjList=new LinkedList[vertices];
		
		for(int i=0; i<vertices; i++) {
			adjList[i]=new LinkedList();
		}
	}
	
	static void addEdge(int v, int w) {
		adjList[v].add(w);
	}
	
	static void traversal(int start, Boolean[] visited) {
		Queue<Integer> q=new LinkedList<Integer>();

		visited[start]=true;		
		q.add(start);
		
		while(!q.isEmpty()) {
			start=q.poll();
			System.out.print(start+" ");

			ListIterator<Integer> list=adjList[start].listIterator();
			
			while(list.hasNext()) {
				int n=list.next();
				
				if(!visited[n]) {
					visited[n]=true;
					q.add(n);
				}
			}
		}
	}
}

public class BFSTraversal {
	public static void main(String args[]) {
		int vertices=4;
		BFSGraph g = new BFSGraph(vertices);
		BFSGraph.addEdge(0,1);
		BFSGraph.addEdge(0,2);
		BFSGraph.addEdge(1,2);
		BFSGraph.addEdge(2,0);
		BFSGraph.addEdge(2,3);
		BFSGraph.addEdge(3,3);
		
		Boolean visited[] = new Boolean[vertices];
		int start=2;
		
		for(int i=0; i<vertices; i++)
			visited[i]=false;
		
		BFSGraph.traversal(start,visited);
	}
}
