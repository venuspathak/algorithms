package practiceJava;

public class RuntimePolymorphism {

	public void run() {
		System.out.println("Run method of parent");
	}

	public static void main(String args[]) {
		RuntimePolymorphism obj = new child();
		obj.run();
	}
}

class child extends RuntimePolymorphism {
	public void run() {
		System.out.println("Run method of child");
	}
}
