package practiceJava;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;

public class Amazon1 {

	static void inputToFunction(int dest, int del, List<List<Integer>> list) {
		List<List<Integer>> printList=countDeliveries(dest,del,list);
		
		for(int i=0; i<printList.size(); i++) {
			System.out.println(printList.get(i));
		}
	}
	
	static List<List<Integer>> countDeliveries(int dest, int del, List<List<Integer>> list) {
		Double[] dist = new Double[list.size()];
		HashMap<Double,List<Integer>> map = new HashMap<Double,List<Integer>>();
		
		for(int i=0; i<list.size(); i++) {
			int first = (list.get(i)).get(0);
			int firstSq=first*first;
			int second = (list.get(i)).get(1);
			int secondSq=second*second;
			
			double sqrt=Math.sqrt(firstSq+secondSq);
			dist[i]=sqrt;
			
			map.put(dist[i],list.get(i));
		}
		
		Arrays.sort(dist);
		
		List<List<Integer>> finalList = new ArrayList<List<Integer>>();
		
		for(int i=0; i<del; i++){
			finalList.add(map.get(dist[i]));
		}		
		return finalList;		
	}
	
	public static void main(String args[]) {
		List<List<Integer>> listToSend = new ArrayList<List<Integer>>();
		List<Integer> list1 = new ArrayList<Integer>();
		List<Integer> list2 = new ArrayList<Integer>();
		List<Integer> list3 = new ArrayList<Integer>();

		list1.add(3);
		list1.add(2);
		list2.add(1);
		list2.add(5);
		list3.add(2);
		list3.add(7);
		
		listToSend.add(list1);
		listToSend.add(list2);
		listToSend.add(list3);

		
		inputToFunction(3,2,listToSend);
	}
}
