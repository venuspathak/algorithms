package practiceJava;

class ThreadClass extends Thread {
	@Override
	public void run() {
		System.out.println("Running thread: " + Thread.currentThread().getId());
	}
}

public class ThreadUsingThreadClass {
	public static void main(String args[]) {
		for (int i = 0; i < 10; i++) {
			ThreadClass obj = new ThreadClass();
			obj.start();
		}
	}
}
