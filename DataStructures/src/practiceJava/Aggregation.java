package practiceJava;

import java.util.List;
import java.util.ArrayList;

class Student {
	int id;
	String name;
	String dept;

	Student(int id, String name, String dept) {
		this.id = id;
		this.name = name;
		this.dept = dept;
	}
}

class Department {
	String name;
	private List<Student> students;

	Department(String name, List<Student> students) {
		this.name = name;
		this.students = students;
	}

	public List<Student> getStudents() {
		return students;
	}
}

class Institute {
	String name;
	private List<Department> deptts;

	Institute(String name, List<Department> deptts) {
		this.name = name;
		this.deptts = deptts;
	}

	int countStudents() {
		int count = 0;
		List<Student> listStudents = new ArrayList<Student>();
		for (Department d : deptts) {
			listStudents = d.getStudents();

			for (Student s : listStudents) {
				count++;
			}
		}
		return count;
	}
}

public class Aggregation {
	public static void main(String args[]) {
		Student s1 = new Student(1, "Venus", "CS");
		Student s2 = new Student(2, "Ajay", "CS");
		Student s3 = new Student(3, "Suman", "EE");
		Student s4 = new Student(3, "Sushil", "EE");

		List<Student> studentListCS = new ArrayList<Student>();
		studentListCS.add(s1);
		studentListCS.add(s2);

		List<Student> studentListEE = new ArrayList<Student>();
		studentListEE.add(s3);
		studentListEE.add(s4);

		Department deptt1 = new Department("CS", studentListCS);
		Department deptt2 = new Department("EE", studentListEE);

		List<Department> depttList = new ArrayList<Department>();
		depttList.add(deptt1);
		depttList.add(deptt2);

		Institute ins = new Institute("Guru Nanak Dev University", depttList);

		System.out.println(ins.countStudents());
	}
}
