package practiceJava;

class ClassWithMethods {
	void run(int a) {
		System.out.println("Method with 1 int argument: " + a);
	}

	void run(int a, int b) {
		System.out.println("Method with 2 int arguments: " + a+","+b);
	}

	void run(float a, int b) {
		System.out.println("Method with 1 float and 1 int argument: " + a+","+b);
	}

	void run(int a, float b) {
		System.out.println("Method with 1 int and 1 float argument: " + a+","+b);
	}
}

public class CompileTimePolymorphism {
	public static void main(String args[]) {
		ClassWithMethods obj = new ClassWithMethods();
		obj.run(5);
		obj.run(5, 10);
		obj.run(5.5f, 10);
		obj.run(10, 5.5f);
	}
}
