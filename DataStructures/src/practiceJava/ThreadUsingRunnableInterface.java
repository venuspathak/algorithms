package practiceJava;

public class ThreadUsingRunnableInterface implements Runnable {

	public void run() {
		System.out.println("Running thread :" + Thread.currentThread().getId());
		String s1="javatpoint";  
		System.out.println(s1.substring(2,4));
	}

	public static void main(String args[]) {
		for (int i = 0; i < 10; i++) {
			Thread obj = new Thread(new ThreadUsingRunnableInterface());
			obj.start();
		}
	}
}
