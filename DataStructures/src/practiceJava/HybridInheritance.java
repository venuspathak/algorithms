package practiceJava;

interface A {
	public void printFirst();
}

interface B extends A {
	public void printFirst();
	public void printSecond();
}

interface C extends A {
	public void printThird();
}

interface D extends B, C {
	public void printThird();
	public void printFourth();
}

public class HybridInheritance implements D {
	public void printFirst() {
		System.out.println("First");
	}

	public void printSecond() {
		System.out.println("Second");
	}

	public void printThird() {
		System.out.println("Third");
	}

	public void printFourth() {
		System.out.println("Fourth");
	}

	public static void main(String args[]) {
		HybridInheritance obj = new HybridInheritance();
		obj.printFirst();
		obj.printSecond();
		obj.printThird();
		obj.printFourth();
	}
}
