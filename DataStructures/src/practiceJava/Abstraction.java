package practiceJava;

abstract class Shape {
	String color;

	// Constructor
	Shape(String color) {
		System.out.println("Shape constructor called");
		this.color = color;
	}

	// Concrete method
	String getColor() {
		return color;
	}

	abstract double area();

	public abstract String toString();
}

class Square extends Shape {
	int len;

	Square(String color, int len) {
		super(color);
		System.out.println("Square constructor called");
		this.len = len;
	}
	
	@Override
	double area() {
		return len*len;
	}
	
	@Override
	public String toString() {
		return "Square's color is: "+super.color+" and area is: "+area();
	}
}

class Rectangle extends Shape {
	int len, br;
	
	Rectangle(String color, int len, int br) {
		super(color);
		System.out.println("Rectangle constructor called");
		this.len=len;
		this.br=br;
	}
	
	@Override
	double area() {
		return len*br;		
	}
	
	@Override
	public String toString() {
		return "Rectangle's color is: "+super.color+" and area is: "+area();
	}
}

public class Abstraction {
	public static void main(String args[]) {
	Square s = new Square("Pink", 4);
	Rectangle r = new Rectangle("Blue", 5, 6);
	
	System.out.println(s.toString());
	System.out.println(r.toString());
	}
}
