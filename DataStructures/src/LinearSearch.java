
public class LinearSearch {	
	
	int linearSearch(int array[], int element) {
		for (int index = 0; index < array.length; index++)
		{
			if (element == array[index])
				return index;
		}
		return -1;
	}
	
	public static void main(String args[]) {
		LinearSearch obj = new LinearSearch();
		int arr[] = {10,467765,77,902,20,1};
		
		int location = obj.linearSearch(arr, 77);
		if (location == -1)
			System.out.println("Element not found");
		else 
			System.out.println("Element found at: "+location);		
	}
}
