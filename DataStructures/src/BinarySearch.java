
public class BinarySearch {

	int binarySearch(int arr[], int x)
	{
		int start = 0;
		int end = arr.length - 1;
		
		while (start <= end)
		{
			int mid = start + (end - start)/2;
			
			if (arr[mid] == x) // If the search element is present in the middle of the array, then return the middle element
				return mid;
			else if (arr[mid] > x) // If the search element is smaller than the middle of the array, then search in the left sub-array
				end = mid - 1;
			else // If the search element is smaller than the middle of the array, then search in the left sub-array
				start = mid + 1;
		}
		return -1; // If search element is not found in the array
	}
	
	public static void main(String args[])
	{
		BinarySearch obj = new BinarySearch();
		int arr[] = {1,2,50,100,657}; // Create a sorted array
		int searchElement = 100;
		
		int result = obj.binarySearch(arr, searchElement);
		if (result == -1)
			System.out.println("Element not found");
		else 
			System.out.println("Element found at location: " + result);
	}
}
