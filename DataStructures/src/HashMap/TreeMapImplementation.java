package HashMap;

import java.util.TreeMap;

public class TreeMapImplementation {
	
	static void insert(TreeMap<Integer, String> map) {
		map.put(1, "One");
		map.put(0, "Zero");
		map.put(3, "Three");
		map.put(2, "Two");
		map.put(-1, "Minus One");
		map.put(-2, "Minus Two");
		map.put(-3, "Minus Three");	
	}
	
	static void print(TreeMap<Integer, String> map) {
		for(int key:map.keySet()) {
			String value=map.get(key);
			System.out.println("Key: "+key+" and Value: "+value);
		}
	}
	
	public static void main(String args[]) {
		TreeMap<Integer,String> map = new TreeMap<Integer,String>();
		
		insert(map);
		
		print(map);
	}
}
