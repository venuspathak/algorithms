package Trees;
import java.util.Stack;

 class Node20 {
	int data;
	Node20 left;
	Node20 right;
	
	Node20(int data) {
		this.data=data;
		this.left=null;
		this.right=null;
	}
}

public class InorderTraversalWithoutRecursion {

	
	static void inorder(Node20 root) {
		Node20 current=root;
		Stack<Node20> s=new Stack<Node20>();
		
		while(!s.isEmpty() || current!=null) {
			
			while(current!=null) {
				s.push(current);
				current=current.left;
			}
			
			current=s.pop();
			System.out.print(current.data+" ");
			
			current=current.right;
		}
	}
	
	public static void main(String args[]) {
		Node20 root=new Node20(1);
		root.left=new Node20(2);
		root.right=new Node20(3);
		root.left.left=new Node20(4);
		root.left.right=new Node20(5);
		
		inorder(root);
	}
}
