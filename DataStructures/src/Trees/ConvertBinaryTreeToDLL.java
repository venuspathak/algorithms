package Trees;

class DLLNode {
	int data;
	DLLNode left,right;
	
	DLLNode(int data) {
		this.data=data;
		left=right=null;
	}
}
public class ConvertBinaryTreeToDLL {
	static DLLNode head;
	
	static void convertToDLL(DLLNode root) {
		if(root==null)
			return;
		
		// Reverse Inorder Traversal		
		// Traverse the right tree
		convertToDLL(root.right);
		
		// Right of current node should point to head
		root.right=head;
		// Left of head should point to current node
		if(head!=null)
			head.left=root;
		// Change head
		head=root;		
		
		// Traverse the left tree
		convertToDLL(root.left);
	}
	
	static void printDLL(DLLNode head) {
		DLLNode current=head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.right;
		}
	}
	
	public static void main(String args[]) {
		DLLNode root=new DLLNode(10);
		root.left=new DLLNode(12);
		root.right=new DLLNode(15);
		root.left.left=new DLLNode(25);
		root.left.right=new DLLNode(30);
		root.right.left=new DLLNode(36);
		
		convertToDLL(root);
		
		printDLL(head);
	}
}
