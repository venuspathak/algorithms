
public class QuickSortUsingLastElementAsPivot {

	// This partitions the array using the last element as pivot
	int partition(int array[], int low, int high) 
	{
		int pivot = array[high];
		// Choosing the last element of the array as pivot
		int i = (low-1); // Index of smaller element
		
		for(int j=low; j<high; j++) 
		{
			if (array[j] < pivot)
			{
				i++;
				
				// Swap array[i] and array[j]
				int temp = array[i];
				array[i] = array[j];
				array[j] = temp;
			}
		}
		
		// Swap array[i+1] and array[high] (or pivot)
		int temp = array[i+1];
		array[i+1] = array[high];
		array[high] = temp;
		
		// At the end of this method, the pivot is placed in the right position meaning that the elements on its left are smaller than it 
		// and the elements on its right are greater than it
		return i+1;
	}	
	
	// This finds the partition and then sort the left and right sub-arrays
	void sort(int array[], int low, int high)
	{
		if (low < high) 
		{
		// Find the index of the partitioning element
		int partition=partition(array, low, high);
		
		sort(array, low, partition-1);
		sort(array, partition+1, high);
		}
	}
	
	// Driver program
	public static void main(String args[])
	{
		QuickSortUsingLastElementAsPivot obj = new QuickSortUsingLastElementAsPivot();
		int arr[] = {10, 7, 8, 9, 1, 5};
		int n=arr.length;
		
		obj.sort(arr, 0, n-1);
		
		System.out.println("The sorted array is:");
		for(int i=0; i<arr.length; i++)
			System.out.print(arr[i]+" ");
	}
}
