package LinkedLists;
import java.util.HashSet;

public class ListQuestion2_1 {
	//Node head;
	
	static class Node {
		int data;
		Node next;
		
		Node(int val) {
			data=val;
			next=null;
		}
	}
	
	public static Node removeDuplicate(Node head) {	
		HashSet<Integer> hs = new HashSet<Integer>();
		Node current = head;
		Node previous = null;
		
		while(current!=null) {
			if(hs.contains(current.data)) {
				// Remove this node
				previous.next=current.next;
			} 
			else {
				// Put this node's value in HashSet
				hs.add(current.data)
;				previous=current;
			}
			current=current.next;
		}
		return head;
	}
	
	public static void printList(Node head) {		
		while(head!=null) {
			System.out.print(head.data+" ");
			head=head.next;
		}
	}
	
	public static void main(String args[]) {
		Node head=new Node(10);
		head.next=new Node(10);
		head.next.next=new Node(10);
		head.next.next.next=new Node(11);
		head.next.next.next.next=new Node(12);
		head.next.next.next.next.next=new Node(11);
		
		printList(head);
		head=removeDuplicate(head);
		System.out.println();
		printList(head);
	}
}
