package LinkedLists;

import LinkedLists.LinkedList.Node;

public class ListQuestion2_4_Again {

	static class Node5 {
		int data;
		Node5 next;
		
		Node5(int data) {
			this.data = data;
			this.next = null;
		}
	}

	static Node5 partition(Node5 head, int k) {
	    /* Let us initialize start and tail nodes of  
	    new list */
	    Node5 tail = head;  
	  
	    // Now iterate original list and connect nodes  
	    Node5 curr = head;  
	    while (curr != null)  
	    {  
	        Node5 next = curr.next;  
	        if (curr.data < k)  
	        {  
	            /* Insert node at head. */
	            curr.next = head;  
	            head = curr;  
	        }  
	  
	        else // Append to the list of greater values  
	        {  
	            /* Insert node at tail. */
	            tail.next = curr;  
	            tail = curr;  
	        }  
	        curr = next;  
	    }  
	    tail.next = null;  
	  
	    // The head has changed, so we need  
	    // to return it to the user.  
	    return head;

	}
	
	/* Function to print linked list */
	static void printList(Node5 head)  
	{  
	    Node5 temp = head;  
	    while (temp != null)  
	    {  
	        System.out.print(temp.data + " ");  
	        temp = temp.next;  
	    }  
	} 
	
	
	public static void main(String args[]) {
		// 3,5,8,2,10,2,1
		// 1,2,2,3,5,8,10
		Node5 head = new Node5(3);
		head.next = new Node5(5);
		head.next.next = new Node5(8);
		head.next.next.next = new Node5(2);
		head.next.next.next.next = new Node5(10);
		head.next.next.next.next.next = new Node5(2);
		head.next.next.next.next.next.next = new Node5(1);
		
		head = partition(head,5);
		printList(head);
	}
}
