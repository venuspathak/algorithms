package LinkedLists;

public class ListQuestion2_2_Again {

	static class Node3 {
		Node3 next;
		int data;
		
		Node3(int data) {
			this.data = data;
			this.next = null;
		}
		
		static void printKthFromLast(Node3 head, int k) {
			Node3 current = head;
			int size=0;
			
			// Find size of the list
			while(current!=null) {
				size++;
				current = current.next;
			}
			
			System.out.println("Size of the list is: "+size);

			
			int front = (size-k)+1;
			current=head;
			for(int i=1; i<front; i++) {
				current=current.next;
			}
			System.out.println("kth element front is: "+current.data);			
		}
		
		public static void main(String args[]) {
			Node3 head = new Node3(1);
			head.next = new Node3(2);
			head.next.next = new Node3(3);
			head.next.next.next = new Node3(4);
			head.next.next.next.next = new Node3(5);
			head.next.next.next.next.next = new Node3(6);
			head.next.next.next.next.next.next = new Node3(7);
			
			printKthFromLast(head,3);
		}
	}
}
