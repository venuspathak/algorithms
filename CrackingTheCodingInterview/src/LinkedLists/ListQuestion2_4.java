package LinkedLists;

public class ListQuestion2_4 {
	static class Node {
		int data;
		Node next;

		Node(int val) {
			data = val;
			next = null;
		}
	}
	
	public static void partition(Node head, int k) {
		Node current=head;
		Node tail=head;
		
		while(current!=null) {
			if(current.data<k) { // Add node to the head
				
			}
			else { // Add node to the tail
				
				
			}
		}
	}

	public static void main(String args[]) {
		Node head=new Node(3);
		head.next=new Node(5);
		head.next.next=new Node(8);
		head.next.next.next=new Node(2);
		head.next.next.next.next=new Node(10);
		head.next.next.next.next.next=new Node(2);
		head.next.next.next.next.next.next=new Node(1);
		
		partition(head,5);
	}
}
