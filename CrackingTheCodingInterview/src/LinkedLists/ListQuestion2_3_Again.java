package LinkedLists;

import LinkedLists.LinkedList.Node;

public class ListQuestion2_3_Again {

	static class Node4 {
		Node4 next;
		int data;
		
		Node4(int data) {
			this.data = data;
			this.next = null;
		}
		
		static void deleteNode(Node4 head, Node4 nodeToDel) {
			Node4 current = head;
			
			// If node to delete is the first node
			if(nodeToDel==head)
				return;
			// If node is the last node
			if(nodeToDel.next==null)
				return;
			
			Node4 temp = nodeToDel.next;
			nodeToDel.data = temp.data;
			nodeToDel.next = temp.next;
		}
	
		public static void main(String args[]) {
			Node4 head = new Node4(1);
			head.next = new Node4(2);
			head.next.next = new Node4(3);
			head.next.next.next = new Node4(4);
			head.next.next.next.next = new Node4(5);
			head.next.next.next.next.next = new Node4(6);
			head.next.next.next.next.next.next = new Node4(7);			
		}
	}
}
