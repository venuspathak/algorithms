package LinkedLists;

public class LinkedList_Reverse {

	static class Node7 {
		int data;
		Node7 next;
		
		Node7(int data) {
			this.data=data;
		}
	}
	
	static Node7 reverse(Node7 head) {
		Node7 prev = null;
		Node7 current = head;
		Node7 next;
		
		while(current!=null) {
			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}
		head = prev;
		return head;
	}
	
	static void print(Node7 head) {
		Node7 current = head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
		System.out.println();
	}
	
	public static void main(String args[]) {
		Node7 head = new Node7(7);
		head.next = new Node7(1);
		head.next.next = new Node7(6);
		
		print(head);	
		head = reverse(head);
		print(head);
	}
}
