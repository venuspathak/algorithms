package LinkedLists;

public class LinkedList_Circular {
	
	static class Node10 {
		int data;
		Node10 next;
		
		Node10(int data) {
			this.data=data;
			this.next=null;
		}
	}
	
	static void print(Node10 head) {
		Node10 current = head;
		
		do {
			System.out.print(current.data+" ");
			current=current.next;
		} while(current!=head);
	}
	
	public static void main(String args[]) {
		Node10 head1 = new Node10(3);
		head1.next = new Node10(6);
		head1.next.next = new Node10(9);
		head1.next.next.next = new Node10(15);
		head1.next.next.next.next = head1;	
		
		print(head1);
	}

}
