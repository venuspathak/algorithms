package LinkedLists;

public class List_Duplicate_Unsorted {

	static class Node12 {
		int data;
		Node12 next;
		
		Node12(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	static Node12 removeDuplicates(Node12 head) {
		Node12 current1 = head;
		Node12 current2=null;
		
		if(current1==null) {
			return null;
		}
		
		while(current1!=null && current1.next!=null) {	
			current2=current1;
			while(current2.next!=null) {
				if(current1.data==current2.next.data) {
					Node12 temp=current2.next.next;
					current2.next=null;
					current2.next=temp;
				} else {
					current2=current2.next;
				}
			}
			current1=current1.next;
		}
		return head;
	}
	
	static void traverse(Node12 head) {
		Node12 current = head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}
	
	public static void main(String args[]) {
		Node12 head = new Node12(10);
		head.next = new Node12(12);
		head.next.next = new Node12(11);
		head.next.next.next = new Node12(11);
		head.next.next.next.next = new Node12(12);
		head.next.next.next.next.next = new Node12(11);
		head.next.next.next.next.next.next = new Node12(10);
		
		traverse(head);
		System.out.println("");
		removeDuplicates(head);
		System.out.println("");
		traverse(head);
	}
}