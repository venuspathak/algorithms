package LinkedLists;

public class ListQuestion2_3 {
	//static Node head;
	
	static class Node {
		int data;
		Node next;
		
		Node(int val) {
			data=val;
			next=null;
		}
	}
	
	public static void printList(Node head) {
		Node current=head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}
	
	public static void delNode(Node node, Node head) {
		if(node.next==null) // For last node, for head when there is just one node
			return;
		if(node==head) {
			return;
		}
		
		Node temp=node.next;
		node.data=temp.data;
		node.next=temp.next;		
	}
	
	public static void main(String args[]) {
		Node head = new Node(1);
		head.next=new Node(2);
		head.next.next=new Node(3);
		head.next.next.next=new Node(4);
		head.next.next.next.next=new Node(5);
		head.next.next.next.next.next=new Node(6);
		head.next.next.next.next.next.next=new Node(7);
		
		printList(head);
		delNode(head.next.next.next.next.next, head);
		System.out.println();
		printList(head);
		
	}
}
