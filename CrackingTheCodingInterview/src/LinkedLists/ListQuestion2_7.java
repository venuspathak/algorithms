package LinkedLists;
import java.util.HashSet;

public class ListQuestion2_7 {
	
	static class Node9 {
		int data;
		Node9 next;
		
		Node9(int data) {
			this.data=data;
			this.next=null;
		}
	}
	
	static void intersection(Node9 head1, Node9 head2) {
		Node9 current1 = head1;
		Node9 current2 = head2;
		
		HashSet<Integer> set = new HashSet<Integer>();
		
		while(current1!=null) {
			set.add(current1.data);
			current1=current1.next;
		}
		
		boolean found = false;
		while(current2!=null) {
			if(set.contains(current2.data)) {
				found = true;
				System.out.println("Intersecting node is: "+current2.data);
				break;
			}
			current2=current2.next;
		}
		
		if(found==false) {
			System.out.println("There is no intersecting node");
		}
	}
	
	public static void main(String args[]) {
		Node9 head1 = new Node9(3);
		head1.next = new Node9(6);
		head1.next.next = new Node9(9);
		head1.next.next.next = new Node9(15);
		head1.next.next.next.next = new Node9(30);
		
		Node9 head2 = new Node9(10);
		head2.next = new Node9(15);
		head2.next.next = new Node9(30);
		
		intersection(head1,head2);
	}

}
