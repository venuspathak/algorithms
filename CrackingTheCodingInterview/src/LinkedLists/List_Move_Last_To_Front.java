package LinkedLists;

public class List_Move_Last_To_Front {

	static class Node14 {
		int data;
		Node14 next;
		
		Node14(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	static Node14 move(Node14 head) {
		Node14 secLast=null;
		Node14 last=null;
		Node14 current = head;
		
		if(head==null || head.next==null) {
			return null;
		}
		
		while(current.next!=null) {
			secLast=current;
			last=current.next;
			current=current.next;
		}
		
		// Now current has second last node
		// current.next is the last node
		//Node14 add = current.next;
		secLast.next=null;
		last.next=head;
		head=last;

		return head;
	}
	
	static void traverse(Node14 head) {
		Node14 current = head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}
	
	public static void main(String args[]) {
		Node14 head = new Node14(11);
		head.next = new Node14(11);
		head.next.next = new Node14(11);
		head.next.next.next = new Node14(21);
		head.next.next.next.next = new Node14(43);
		head.next.next.next.next.next = new Node14(43);
		head.next.next.next.next.next.next = new Node14(60);
		
		traverse(head);
		System.out.println();
		head=move(head);
		traverse(head);
	}
}
