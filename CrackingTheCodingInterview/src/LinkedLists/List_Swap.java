package LinkedLists;

public class List_Swap {

	static class Node13 {
		int data;
		Node13 next;
		
		Node13(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	static Node13 swap(Node13 head) {
		Node13 current = head;
		
		while(current!=null && current.next!=null) {
			int temp=current.data;
			current.data=current.next.data;
			current.next.data=temp;
			current=current.next.next;
		}
		return head;
	}
	
	static void traverse(Node13 head) {
		Node13 current = head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}
	
	public static void main(String args[]) {
		Node13 head = new Node13(1);
		head.next = new Node13(2);
		head.next.next = new Node13(3);
		head.next.next.next = new Node13(4);
		head.next.next.next.next = new Node13(5);
		head.next.next.next.next.next = new Node13(6);
		head.next.next.next.next.next.next = new Node13(7);
		
		traverse(head);
		System.out.println("");
		head=swap(head);
		System.out.println("");
		traverse(head);
	}
}
