package LinkedLists;

public class List_String_Palindrome {

	static class Node6 {
		String data;
		Node6 next;
		
		Node6(String data) {
			this.data=data;
			this.next=null;
		}
	}
	
	static String appendToString(Node6 head) {
		String str="";
		Node6 current = head;
		
		while(current!=null) {
			str=str+current.data;
			current=current.next;
		}
		return str;
	}
	
	static boolean isPalindrome(String str) {
		int length=str.length();
		
		for(int i=0; i<length/2; i++) {
			if(str.charAt(i)==str.charAt(length-(i+1))) {
				continue;
			}
			else
				return false;
		}
		return true;
	}
	
	public static void main(String args[]) {
		Node6 head = new Node6("a");
		head.next = new Node6("bc");
		head.next.next = new Node6("d");
		head.next.next.next = new Node6("dcb");
		head.next.next.next.next = new Node6("a");
		
		String result = appendToString(head);
		System.out.println(result);
		
		Boolean b = isPalindrome(result);
		System.out.println(b);
	}
}
