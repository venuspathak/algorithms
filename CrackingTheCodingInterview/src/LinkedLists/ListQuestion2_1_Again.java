package LinkedLists;

import java.util.HashSet;

public class ListQuestion2_1_Again {

	static class Node2 {
		Node2 next;
		int data;
		
		Node2(int data) {
			this.next=null;
			this.data=data;
		}
	}
	
	static void print(Node2 head) {
		Node2 current = head;
		while(current!=null) {
			System.out.print(current.data+" ");
			current = current.next;
		}
	}
	
	static Node2 removeDuplicates(Node2 head) {
		HashSet<Integer> set = new HashSet<Integer>();
		Node2 current = head;
		Node2 previous = null;
		
		while(current!=null) {
			if(set.contains(current.data)) {
				previous.next = current.next;				
			} else {
				set.add(current.data);
				previous = current;
			}
			current = current.next;
		}
		
		return head;
	}
	
	public static void main(String args[]) {		
		Node2 head = new Node2(10);
		head.next = new Node2(10);
		head.next.next = new Node2(10);
		head.next.next.next = new Node2(11);
		head.next.next.next.next = new Node2(12);
		head.next.next.next.next.next = new Node2(11);
		
		print(head);
		removeDuplicates(head);
		System.out.println();
		print(head);
	}
}
