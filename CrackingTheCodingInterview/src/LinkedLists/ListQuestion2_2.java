package LinkedLists;

public class ListQuestion2_2 {
	//Node head;
	
	static class Node {
		int data;
		Node next;
		
		Node(int val) {
			data=val;
			next=null;
		}
	}
	
	public static void printKthFromLast(Node head, int k) {
		Node current=head;
		int len=0;
		
		// Find the length of the linked list
		while(current!=null) {
			len++;
			current=current.next;
		}		
		System.out.println("Length of the linked list is: "+len);
		
		if(k>len)
			return;
				
		// The kth element to access from the start would be (length-k)+1
		current=head;
		int target=(len-k)+1;
		for(int i=1; i<target; i++) {
			current=current.next;
		}
		System.out.println(current.data);
	}
	
	public static void main(String args[]) {
		Node head = new Node(1);
		head.next=new Node(2);
		head.next.next=new Node(3);
		head.next.next.next=new Node(4);
		head.next.next.next.next=new Node(5);
		head.next.next.next.next.next=new Node(6);
		head.next.next.next.next.next.next=new Node(7);
		
		printKthFromLast(head,3);
	}
}
