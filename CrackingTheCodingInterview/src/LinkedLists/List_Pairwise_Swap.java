package LinkedLists;

public class List_Pairwise_Swap {

	static class Node14 {
		int data;
		Node14 next;
		
		Node14(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	static Node14 swap(int x, int y, Node14 head) {		
		// If x or y is same, then return
		if(x==y)
			return null;
		
		// Find x in the list, keep track of the x's previous node
		Node14 prevX=null;
		Node14 currX=head; 
		
		while(currX!=null && currX.data!=x) {
			prevX=currX;
			currX=currX.next;
		}
		
		// Find y in the list, keep track of the y's previous node
		Node14 prevY=null;
		Node14 currY=head;
		
		while(currY!=null && currY.data!=y) {
			prevY=currY;
			currY=currY.next;
		}
		
		// If x or y is not present in the list, return
		if(currX==null || currY==null) {
			return null;
		}
		
		// If x is not the head of list
		if(prevX!=null)
			prevX.next=currY;
		else
			head=currY;
		
		// If y is not the head of list
		if(prevY!=null)
			prevY.next=currX;
		else
			head=currX;
		
		// Swap x and y
		Node14 temp=currX.next;
		currX.next=currY.next;
		currY.next=temp;
		
		return head;
	}
	
	static void traverse(Node14 head) {
		Node14 current = head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}
	
	public static void main(String args[]) {
		Node14 head = new Node14(7);
		head.next = new Node14(6);
		head.next.next = new Node14(5);
		head.next.next.next = new Node14(4);
		head.next.next.next.next = new Node14(3);
		head.next.next.next.next.next = new Node14(2);
		head.next.next.next.next.next.next = new Node14(1);
		
		traverse(head);
		System.out.println();
		head=swap(4,3,head);
		traverse(head);
		System.out.println();

	}
}
