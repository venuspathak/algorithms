package LinkedLists;

public class List_Segregate_Even_Odd {

	static class Node14 {
		int data;
		Node14 next;
		
		Node14(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	static void segregate(Node14 head) {
		Node14 evenStart=null;
		Node14 evenEnd=null;
		Node14 oddStart=null;
		Node14 oddEnd=null;
		Node14 current=head;
		
		while(current!=null) {
			
			if(current.data%2==0) {
				System.out.println("The node "+current.data+" is even");
				
				if(evenStart==null) {
					evenStart=current;
					evenEnd=evenStart;
				}else {
					evenEnd.next=current;
					evenEnd=evenEnd.next;
				}
				
			} else {
				System.out.println("The node "+current.data+" is odd");

				if(oddStart==null) {
					oddStart=current;
					oddEnd=oddStart;
				}else {
					oddEnd.next=current;
					oddEnd=oddEnd.next;
				}
			}
			current=current.next;
		}
		
		if(evenStart==null || oddStart==null) {
			return;
		}
		
		evenEnd.next=oddStart;
		oddEnd.next=null;
		head=evenStart;
	}
	
	static void traverse(Node14 head) {
		Node14 current = head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
		System.out.println();
	}
	
	public static void main(String args[]) {
		Node14 head = new Node14(0);
		head.next = new Node14(2);
		head.next.next = new Node14(3);
		head.next.next.next = new Node14(6);
		head.next.next.next.next = new Node14(8);
		head.next.next.next.next.next = new Node14(10);
		head.next.next.next.next.next.next = new Node14(11);
		
		traverse(head);
		segregate(head);
		traverse(head);
	}
}
