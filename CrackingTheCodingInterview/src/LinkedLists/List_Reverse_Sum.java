package LinkedLists;

public class List_Reverse_Sum {

	static class Node8 {
		int data;
		Node8 next;
		
		Node8 (int data) {
			this.data=data;
		}
	}
	
	static Node8 sum(Node8 head1, Node8 head2) {
		Node8 res = null;
		Node8 prev = null;
		Node8 temp = null;
		int sum=0;
		int carry=0;
		
		while(head1!=null || head2!=null) {
			// Calculate sum
			sum = carry + ((head1!=null) ? head1.data : 0) + ((head2!=null) ? head2.data : 0);
			
			// Update carry
			carry = (sum>=10) ? 1 : 0;
			
			// Update sum if it is greater than 10
			sum = sum % 10;
			
			// Create a new node with sum as data
			temp = new Node8(sum); 
			
			if(res==null) {
				res=temp;
			}
			else {
				prev.next = temp;
			}
			prev = temp;
			
			if(head1!=null) {
				head1 = head1.next;
			}
			if(head2!=null) {
				head2 = head2.next;
			}
		}
		
		if(carry>0) {
			temp.next = new Node8(carry);
		}
		
		return res;
	}
	
	static void print(Node8 head) {
		Node8 current = head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
		System.out.println();
	}
	
	public static void main(String args[]) {
		Node8 head1 = new Node8(7);
		head1.next = new Node8(5);
		head1.next.next = new Node8(9);		
		head1.next.next.next = new Node8(4);		
		head1.next.next.next.next = new Node8(6);		

		Node8 head2 = new Node8(8);
		head2.next = new Node8(4);
		
		Node8 head3;
		
		System.out.print("First Linked List: ");
		print(head1);	
		System.out.print("Second Linked List: ");
		print(head2);	
		
		
		head3 = sum(head1, head2);
		
		System.out.print("Linked List after the sum: ");
		print(head3);
	}
}
