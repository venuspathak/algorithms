package LinkedLists;

public class List_Quicksort {

	static class Node14 {
		int data;
		Node14 next;
		
		Node14(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	static void traverse(Node14 head) {
		Node14 current = head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}
	
	public static void main(String args[]) {
		Node14 head = new Node14(11);
		head.next = new Node14(11);
		head.next.next = new Node14(11);
		head.next.next.next = new Node14(21);
		head.next.next.next.next = new Node14(43);
		head.next.next.next.next.next = new Node14(43);
		head.next.next.next.next.next.next = new Node14(60);
	}
}
