package LinkedLists;

public class LinkedList {
	Node head;
	
	static class Node {
		int data;
		Node next;
		
		Node(int val) {
			data=val;
			next=null;
		}
	}
	
	public static LinkedList insertElement(LinkedList list, int val) {
		Node newNode = new Node(val);
		
		if(list.head==null) {
			list.head=newNode;
			//newNode.next=null;
		} 
		else {
			// Traverse till the end of list and add the element there
			Node last=list.head;
			
			while(last.next!=null) {				
				last=last.next;
			}
			last.next=newNode;
		}
		return list;
	}
	
	public static void printList(LinkedList list) {
		Node current = list.head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}
	
	public static void main(String args[]) {
		LinkedList list = new LinkedList();
		
		list=insertElement(list,1);
		list=insertElement(list,2);
		list=insertElement(list,3);
		list=insertElement(list,4);
		list=insertElement(list,5);
		list=insertElement(list,6);
		list=insertElement(list,7);
		list=insertElement(list,8);
		list=insertElement(list,9);
		list=insertElement(list,10);
		
		printList(list);
	}
}