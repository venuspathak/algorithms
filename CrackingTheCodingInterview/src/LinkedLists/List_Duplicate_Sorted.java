package LinkedLists;

public class List_Duplicate_Sorted {

	static class Node11 {
		int data;
		Node11 next;
		
		Node11(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	static Node11 removeDuplicate(Node11 head) {
		Node11 current = head;
		Node11 temp;
		
		if(current==null) {
			return null;
		}
		
		while(current.next!=null) {
			int data=current.data;
			int nextData=current.next.data;
			
			if(data==nextData) {
				temp=current.next.next;
				current.next=null;
				current.next=temp;
			}
			else {
				current=current.next;
			}
		}
		return head;
	}
	
	static void traverse(Node11 head) {
		Node11 current = head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
	}
	
	public static void main(String args[]) {
		Node11 head = new Node11(11);
		head.next = new Node11(11);
		head.next.next = new Node11(11);
		head.next.next.next = new Node11(21);
		head.next.next.next.next = new Node11(43);
		head.next.next.next.next.next = new Node11(43);
		head.next.next.next.next.next.next = new Node11(60);
		
		traverse(head);
		System.out.println("");
		removeDuplicate(head);
		System.out.println("");
		traverse(head);
	}
}
