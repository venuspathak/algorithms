package LinkedLists;

class Node {
	int val;
	Node next;
	
	Node(int val) {
		this.val=val;
		next=null;
	}
}

public class AddTwoNumbers {
	static Node head1, head2, result, curr;
	static int carry;
	
	static void printList(Node head) {
		while(head!=null) {
			System.out.print(head.val+" ");
			head=head.next;
		}
	}
	
	static void push(int val, int n) {
		Node newNode=new Node(val);
		
		if(n==1) {
			System.out.println("Add to first list");
			newNode.next=head1;
			head1=newNode;
			
		}
		else if(n==2) {
			System.out.println("Add to second list");
			newNode.next=head2;
			head2=newNode;
		}
		else {
			System.out.println("Add to result list");
			newNode.next=result;
			result=newNode;
		}
	}
	
	static void addSameSize(Node head1, Node head2) {
		if(head1==null)
			return;
		
		addSameSize(head1.next, head2.next);
		
		int sum=head1.val+head2.val;
		int carry=sum/10;
		sum=sum%10;
		
		push(sum,3);
	}
	
	static int getSize(Node head) {
		int count=0;
		while(head!=null) {
			count++;
			head=head.next;
		}
		return count;
	}
	
	static void propagateCarry(Node head) {
		if(head!=curr) {
			propagateCarry(head1.next);
			
			int sum=carry+head.val;
			carry=carry/10;
			sum=sum%10;
			
			push(sum,3);
		}
	}
	
	static void addLists(Node head1, Node head2) {		
		if(head1==null) {
			result=head2;
			return;
		}
		
		if(head2==null) {
			result=head1;
			return;
		}
		
		int size1=getSize(head1);
		int size2=getSize(head2);		
	
		if(size1==size2) {
			addSameSize(head1, head2);
		}
		else {
			if(size1<size2) {
				Node temp=head1;
				head1=head2;
				head2=temp;
			}
			
			int diff=Math.abs(size1-size2);
			
			Node temp=head1;
			while(diff>=0) {
				curr=temp;
				temp=temp.next;
			}
			
			addSameSize(curr,head2);
			
			propagateCarry(head1);
		}
		
		if(carry>0)
			push(carry,3);
	}
	
	public static void main(String args[]) {
		head1=null;
		head2=null;
		result=null;
		
		int arr1[]= new int[] {9,9,9};
		int arr2[]= new int[] {1,8};
		
		for(int i=arr1.length-1; i>=0; i--) {
			push(arr1[i],1);
		}
		for(int i=arr2.length-1; i>=0; i--) {
			push(arr2[i],2);
		}
		
		addLists(head1,head2);
		
		printList(result);
	}
}
