package ArraysAndStrings;

import java.util.Arrays;

public class Arrays_Max_Sum {
	// Find the index of the largest element in the array
	// Number of rotations n = length of the array-index
	// Rotate the array by n left rotations
	// Now calculate the sum, it should be the largest
	
	static int findLargest(int arr[]) {
		int largest=arr[0];
		int index=0;
		
		for(int i=0; i<arr.length-1; i++) {
			if (arr[i+1]>largest) {
				largest=arr[i+1];
				index=i+1;
			}
			else {
				continue;
			}
		}
		return index;
	}
	
	static int[] rotate(int[] arr, int n) {
		for(int i=0; i<n; i++) {
			int temp=arr[arr.length-1];
			
			for(int j=arr.length-1; j>0; j--) {
				arr[j]=arr[j-1];
			}		
			arr[0]=temp;
		}		
		return arr;
	}
	
	static int sum(int[] arr) {
		int sum=0;
		
		for(int i=0; i<arr.length; i++) {
			sum=sum+arr[i]*i;
		}		
		return sum;
	}
	
	public static void main(String args[]) {	
		int arr[] = new int[] {8,3,1,2};
		
		// Find index of the largest element
		int n=findLargest(arr);
		System.out.println("Index of the largest element: "+n);
		
		int number_of_rotations=arr.length-1-n;
		
		// Left rotate the array n times
		arr=rotate(arr,number_of_rotations);
		System.out.println("After left rotating the array "+number_of_rotations+" times: "+Arrays.toString(arr));
		
		// Calculate sum
		int result=sum(arr);
		System.out.println("Sum: "+result);
	}	
}
