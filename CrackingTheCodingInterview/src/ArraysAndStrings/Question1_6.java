package ArraysAndStrings;

public class Question1_6 {
	
	String stringCompression(String s) {
		String finalStr="";
		int n=s.length();
		for(int i=0; i<n; i++) {
			System.out.println(i);
		    int count=1;
			
			while(i<n-1 && s.charAt(i)==s.charAt(i+1)) {
				count++;
				i++;
			}
			finalStr=finalStr+(Character.toString(s.charAt(i)))+(Integer.toString(count));
			System.out.println(finalStr);
		}
		
		if(finalStr.length()==s.length())
			return s;
		else 
			return finalStr;
	}
	
	public static void main(String args[]) {
		Question1_6 obj = new Question1_6();
		String s="aabcccccaaa";
		System.out.println(obj.stringCompression(s));
	}
}
