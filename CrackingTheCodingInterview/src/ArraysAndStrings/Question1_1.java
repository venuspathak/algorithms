package ArraysAndStrings;
import java.util.HashMap;
import java.util.Map;

public class Question1_1 {
	boolean checkDuplicates(String str, Map<Character, Integer> map) {
		int breaker = 1;
		for (int i = 0; i < str.length(); i++) {
			if(!map.containsKey(str.charAt(i)))
			map.put(str.charAt(i), breaker);
			else {
				return true;
			}
		}
		return false;
	}

	public static void main(String args[]) {
		Question1_1 obj = new Question1_1();
		String str = "vectorrr";
		Map<Character, Integer> map = new HashMap<Character, Integer>();

		if (obj.checkDuplicates(str,map)) {
			System.out.println("Duplicates are found");
		} else {
			System.out.println("There are no duplicates");
		}
	}
}
