package ArraysAndStrings;

import java.util.Arrays;

public class Array_FindPair {
	
	static boolean findPair(int[] arr, int k) {
		int l=0;
		int r=arr.length-1;
		
		Arrays.sort(arr);
		
		while(l<r) {
			int sum=arr[l]+arr[r];
			if(sum==k) {
				return true;
			}
			else if(sum>k) {
				r--;
			}
			else {
				l++;
			}
		}
		return false;		
	}

	public static void main(String args[]) {
		int arr[] = new int[] {1,4,45,6,10,-8};
		int k=16;
		
		boolean result=findPair(arr,k);
		if(result) {
			System.out.println("Pair found");
		}
		else {
			System.out.println("Pair not found");
		}
	}
}
