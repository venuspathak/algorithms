package ArraysAndStrings;

public class Array_Search {

	static void linearSearch(int arr[], int k) {
		
		boolean found=false;
		for(int i=0; i<arr.length; i++) {
			if(k==arr[i]) {
				System.out.println("Element "+k+" found at array index "+i);
				found=true;
			}
		}
		
		if(found==false) {
			System.out.println("Element "+k+" not found in the array");
		}
	}
	
	static int binarySearch(int arr[], int low, int high, int k) {		
		if(low<=high) {
			int mid=low+(high-low)/2;
			
			if(arr[mid]==k) {
				return mid;
			}
			else if(k<arr[mid]) {		
				return binarySearch(arr,low,mid-1,k);
			}
			else {
				return binarySearch(arr,mid+1,high,k);
			}
		}
		return -1;
	}

	public static void main(String args[]) {
		int arr[] = new int[] {3, 4, 5, 1, 2};
		
		linearSearch(arr,1);
		linearSearch(arr,10);

		int element=3;
		int loc=binarySearch(arr, 0, arr.length-1, element);
		if(loc!=-1) {
			System.out.println("Element "+element+" found at the location "+loc);
		} else {
			System.out.println("Element not found in the array");
		}
	}
}
