package ArraysAndStrings;

public class Question1_8 {
	
	void setZerosInMatrix(int[][] arr, int m, int n) {
		boolean[] row = new boolean[m];
		boolean[] column = new boolean[n];
		
		for(int i=0; i<m; i++) {
			for(int j=0; j<n; j++) {
				if(arr[i][j]==0) {
					row[i]=true;
					column[j]=true;
				}
			}
		}
		
		for(int i=0; i<m; i++) {
			if(row[i]) {
				for(int j=0; j<n; j++) { 
					arr[i][j]=0;
				}
			}
		}
		
		for(int j=0; j<n; j++) {
			if(column[j]) {
				for(int i=0; i<m ; i++) {
					arr[i][j]=0;
				}
			}
		}
	}
	
	int[][] initArray(int[][] arr) {
		arr[0][0]=1;
		arr[1][0]=0;
		arr[2][0]=7;
		arr[0][1]=2;
		arr[1][1]=5;
		arr[2][1]=8;
		arr[0][2]=3;
		arr[1][2]=6;
		arr[2][2]=9;

		return arr;
	}
	
	void printArray(int[][] arr, int m, int n) {
		for(int i=0; i<m; i++) {
			for(int j=0; j<n; j++) {
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	public static void main(String args[]) {
		Question1_8 obj = new Question1_8();
		int[][] arr = new int[3][3];
		
		arr=obj.initArray(arr);
		obj.printArray(arr,3,3);
		obj.setZerosInMatrix(arr,3,3);
		System.out.println("FINAL ARRAY IS: ");
		obj.printArray(arr,3,3);
	}
}
