package ArraysAndStrings;

import java.util.Arrays;

public class Array_RightRotation {

	static void rotate(int[] arr, int n) {
		System.out.println(Arrays.toString(arr));
		
		for(int i=0; i<n; i++) {
			int temp=arr[arr.length-1];
			
			for(int j=arr.length-1; j>0; j--) {
				arr[j]=arr[j-1];
			}
			arr[0]=temp;
		}
		
		System.out.println(Arrays.toString(arr));
	}
	
	public static void main(String args[]) {
	
		int[] arr = new int[]{1,2,3,4,5,6,7};
		rotate(arr, 2);
	}
}