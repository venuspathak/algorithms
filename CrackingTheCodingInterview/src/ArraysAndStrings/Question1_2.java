package ArraysAndStrings;

public class Question1_2 {
	
	boolean permutationOfEachOther(String str1, String str2) {
		if(!(str1.length()==str2.length())) {
			return false;
		}
		char[] str2Array = str2.toCharArray();
		for(int i=0; i<str1.length(); i++) {
			Character c=str1.charAt(i);
			int index = str2.indexOf(c);
			if(index==-1)
				return false;
		}
		return true;
	}
	
	public static void main(String args[]) {
		Question1_2 obj = new Question1_2();
		
		if(obj.permutationOfEachOther("van","nva"))
			System.out.println("Strings are permutations of each other");
		else {
			System.out.println("Strings are not permutations of each other");
		}
	}
}
