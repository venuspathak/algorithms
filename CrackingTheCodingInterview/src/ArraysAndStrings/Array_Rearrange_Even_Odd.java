package ArraysAndStrings;

import java.util.Arrays;

public class Array_Rearrange_Even_Odd {
	
	static int[] rearrange(int[] arr) {
		int length=arr.length;
		int[] temp = new int[length];
		int counter=length-1;
		
		// Sort the array first
		Arrays.sort(arr);
		
		for(int i=length-1; i>=0; i--) {
			if(i%2==0 && i!=0) {
				temp[i-1]=arr[counter];
				counter--;
			}
		}
		
		for(int i=0; i<length+1; i++) {
			if(i%2!=0) {
				temp[i-1]=arr[counter];
				counter--;
			}
		}
		
		return temp;		
	}
	
	public static void main(String args[]) {
		int[] arr = new int[] {1,2,3,4,5,6,7};		
		arr=rearrange(arr);
		System.out.println(Arrays.toString(arr));
	}
}
