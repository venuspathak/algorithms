package ArraysAndStrings;

import java.util.Arrays;

public class Question1_3 {

	void replaceSpaces(String str) {
		str = str.trim();

		int len = str.length();
		int spaceCount = 0;

		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == ' ')
				spaceCount++;
		}
		// System.out.println("Count for the space is: " + spaceCount);

		int newLen = len + 2 * spaceCount;
		// System.out.println("Old length is: " + len);
		// System.out.println("New length is: " + newLen);

		char[] array = str.toCharArray();
		char[] finalArray = new char[newLen];
		int j = newLen - 1;
		for (int i = array.length - 1; i >= 0; i--) {
			// System.out.println("Character: "+array[i]);
			if (array[i] == ' ') {
				// System.out.println("Entered this");
				finalArray[j] = '0';
				finalArray[j - 1] = '2';
				finalArray[j - 2] = '%';
				j = j - 3;
			} else {
				finalArray[j] = array[i];
				j--;
			}
		}
		String finalString = Arrays.toString(finalArray);
		/*
		 * for(int i=0; i<finalArray.length; i++) { System.out.print(finalArray[i]); }
		 */
		System.out.println("After replacements: " + finalString);
	}

	public static void main(String args[]) {
		Question1_3 obj = new Question1_3();
		String str = "Mr John Smith   ";

		obj.replaceSpaces(str);
	}
}
