package ArraysAndStrings;

import java.util.Arrays;

public class Array_LeftRotation_UsingReverse {

	 int[] rotateUsingReverse(int[] arr, int d) {
		int len = arr.length;
		
		reverse(arr,0,d-1);
		reverse(arr,d,len-1);
		reverse(arr,0,len-1);
		return arr;
	}
	
	 void reverse(int arr[], int start, int end) {
		int temp;
		
		while(end>start) {
			temp=arr[start];
			arr[start]=arr[end];
			arr[end]=temp;
			
			start++;
			end--;
		}
	}
	
	public static void main(String args[]) {
		Array_LeftRotation_UsingReverse obj = new Array_LeftRotation_UsingReverse();
	
		int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7};
		int d=2;
		
		System.out.println("Array before rotation "+Arrays.toString(arr));
		arr=obj.rotateUsingReverse(arr, d);
		System.out.println("Array after rotation "+Arrays.toString(arr));
	}
}
