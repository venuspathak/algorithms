package ArraysAndStrings;

public class Question1_9 {
	
	void isSubstring(String s1, String s2) {
		s2=s2+s2;
		System.out.println(s2);
		if(s2.contains(s1))
			System.out.println("s2 is a rotation of s1");
		else 
			System.out.println("s2 is not a rotation of s1");
	}
	
	public static void main(String args[]) {
		Question1_9 obj = new Question1_9();
		String s1="waterbottle";
		String s2="erbottlewat";
		
		obj.isSubstring(s1, s2);
	}
}
