package ArraysAndStrings;

import java.util.Arrays;

public class Array_Reverse {
	
	static int[] reverse(int[] arr) {
		int length=arr.length;
		int[] temp=new int[arr.length];
		int counter=0;
		
		for(int i=length-1; i>=0; i--) {
			temp[counter]=arr[i];
			counter++;
		}
		return temp;
	}
	public static void main(String args[]) {
		
		int[] arr = new int[] {1,2,3,4};
		arr=reverse(arr);
		System.out.println(Arrays.toString(arr));
	}

}
