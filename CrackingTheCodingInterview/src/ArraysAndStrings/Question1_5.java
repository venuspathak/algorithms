package ArraysAndStrings;

public class Question1_5 {

	boolean checkEdit(String s1, String s2) {
		int len1 = s1.length();
		int len2 = s2.length();

		if (Math.abs(len1 - len2) > 1)
			return false;
		else {
			if (s1.contains(s2) || s2.contains(s1))
				return true;
			else {
				int target = s1.length() - 1;
				int count = 0;
				for (int i = 0; i < s1.length(); i++) {
					if (s2.indexOf(s1.charAt(i)) != -1) {
						count++;
					}
				}
				if (count == target)
					return true;
				else
					return false;
			}
		}
	}

	public static void main(String args[]) {
		Question1_5 obj = new Question1_5();

		String s1 = "pale";
		String s2 = "ple";
		String s3 = "pales";
		String s4 = "bale";
		String s5 = "bake";

		System.out.println(obj.checkEdit(s1, s2));
		System.out.println(obj.checkEdit(s3, s1));
		System.out.println(obj.checkEdit(s1, s4));
		System.out.println(obj.checkEdit(s1, s5));
	}
}
