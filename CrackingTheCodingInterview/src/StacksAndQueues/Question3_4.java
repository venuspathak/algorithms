package StacksAndQueues;

import java.util.Stack;

public class Question3_4 {

	static class Queue {
		Stack<Integer> s1= new Stack<Integer>();
		Stack<Integer> s2= new Stack<Integer>();		
	}
	
	static void push(Stack<Integer> s, int k) {
		s.push(k);
	}
	
	static int pop(Stack<Integer> s) {
		if(s.isEmpty()) {
			System.out.println("Stack Underflow");
			System.exit(0);
		}
		int x = s.pop();
		return x;
	}
	
	static void enQueue(Queue q, int k) {
		push(q.s1,k);
	}
	
	static int deQueue(Queue q) {
		int x;
		// Move all elements from s1 to s2
		if(q.s2.isEmpty()) {
			while(!q.s1.isEmpty()) {
				x = pop(q.s1);
				push(q.s2,x);
			}
		}
		// Then pop element from s2
		x = pop(q.s2);
		return x;
	}
	
	public static void main(String args[]) {
		Queue q = new Queue();
		enQueue(q,1);
		enQueue(q,2);
		enQueue(q,3);
		
		System.out.println(deQueue(q));
		System.out.println(deQueue(q));
		System.out.println(deQueue(q));
	}
}
