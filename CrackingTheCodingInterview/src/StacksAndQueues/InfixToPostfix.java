package StacksAndQueues;

import java.util.Stack;

public class InfixToPostfix {
	
	static int findPrecedence(Character ch) {
		switch(ch) {
		case '+':
			System.out.println("Character "+ch+" is addition operator");
			return 1;
		case '-':
			System.out.println("Character "+ch+" is subtraction operator");
			return 1;
		case '*':
			System.out.println("Character "+ch+" is multiplication operator");
			return 2;
		case '/':
			System.out.println("Character "+ch+" is division operator");
			return 2;
		case '^':
			System.out.println("Character "+ch+" is misc operator");
			return 3;
		}
		return -1;
	}
	
	static String convert(String str) {
		String returnStr="";
		Stack<Character> stack=new Stack<Character>();
		
		for(int i=0; i<str.length(); i++) {
			Character ch=str.charAt(i);
			int precedence=findPrecedence(ch);
			
			if(precedence!=-1) {				
				if(stack.isEmpty()) {
					stack.push(ch);
				}
				else {
					int stackPrec=findPrecedence(stack.peek());
					
					if(precedence<=stackPrec) {
						int topPrec=stackPrec;
						while(precedence<=topPrec) {
							Character popped=stack.pop();
							returnStr=returnStr+popped;
							if(!stack.isEmpty()) {
								topPrec=findPrecedence(stack.peek());
							}
							else {
								topPrec=-1;
							}
						}
						stack.push(ch);
					}
					else {
						stack.push(ch);
					}
				}				
			}
			else if(ch=='(') {
				stack.push(ch);				
			}
			else if(ch==')') {
				while(stack.peek()!='(') {
					Character popped=stack.pop();
					returnStr=returnStr+popped;
				}
				stack.pop();
			}
			else {
				returnStr=returnStr+ch;
			}
		}
		
		while(!stack.isEmpty()) {
			returnStr=returnStr+stack.pop();
		}
		return returnStr;
	}

	public static void main(String args[]) {
		String str="a+b*(c^d-e)^(f+g*h)-i";
		
		System.out.println("Infix epression is: "+str);		
		System.out.println("Postfix epression is: "+convert(str));
	}
}
