package StacksAndQueues;

import java.util.Stack;

public class SortStack_UsingRecursion {
	
	static void insertAtBottom(Stack<Integer> s, int x) {
		if(s.isEmpty() || x>s.peek()) {
			s.push(x);
			return;
		}
		
		int temp=s.pop();
		insertAtBottom(s,x);
		
		s.push(temp);
	}
	
	static void sort(Stack<Integer> s) {
		if(s.isEmpty())
			return;
		
		int x=s.pop();
		
		sort(s);
		
		insertAtBottom(s,x);
	}
	
	public static void main(String args[]) {	
        Stack<Integer> s = new Stack<>(); 
        s.push(30); 
        s.push(-5); 
        s.push(18); 
        s.push(14); 
        s.push(-3); 
        
        System.out.println(s);
        sort(s);
        System.out.println(s);
	}
}
