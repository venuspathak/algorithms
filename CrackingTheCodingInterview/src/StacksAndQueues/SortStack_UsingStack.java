package StacksAndQueues;

import java.util.Stack;

public class SortStack_UsingStack {
	
	static Stack<Integer> sort(Stack<Integer> s) {
		Stack<Integer> temp = new Stack<Integer>();
		
		if(s.isEmpty())
			return null;
		
		while(!s.isEmpty()) {
			int x = s.pop();
			
			while(!temp.isEmpty() && x<temp.peek()) {
				s.push(temp.pop());
			}
			temp.push(x);
		}
		
		return temp;
	}

	public static void main(String args[]) {
		Stack<Integer> s = new Stack<Integer>();
		s.push(34); 
	    s.push(3); 
	    s.push(31); 
	    s.push(98); 
	    s.push(92); 
	    s.push(23); 
	    
	    System.out.println(s);
	    s=sort(s);
	    System.out.println(s);
	}
}
