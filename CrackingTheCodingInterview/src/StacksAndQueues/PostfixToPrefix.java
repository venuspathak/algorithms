package StacksAndQueues;

import java.util.Stack; 
  
public class PostfixToPrefix { 
	
	static boolean isOperator(Character ch) {
		switch(ch) {
		case '+':
			System.out.println("Character "+ch+" is addition operator");
			return true;
		case '-':
			System.out.println("Character "+ch+" is subtraction operator");
			return true;
		case '*':
			System.out.println("Character "+ch+" is multiplication operator");
			return true;
		case '/':
			System.out.println("Character "+ch+" is division operator");
			return true;
		}
		return false;
	}
	
	static String convert(String str) {
		String result="";
		Stack<String> stack=new Stack<String>();
		
		for(int i=0; i<str.length(); i++) {
			Character ch=str.charAt(i);
			
			if(isOperator(ch)) {
				String a=stack.pop();
				String b=stack.pop();
				
				String temp=ch+b+a;
				stack.push(temp);
			}
			else {
				stack.push(ch.toString());
			}
		}
		result=stack.pop();
			
		return result;
	}

	public static void main(String args[]) {
		String postfix="ABC/-AK/L-*";
		
		System.out.println("Postfix: "+postfix);
		System.out.println("Prefix: "+convert(postfix));
	}
} 
  
