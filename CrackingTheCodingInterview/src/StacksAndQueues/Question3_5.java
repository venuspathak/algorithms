package StacksAndQueues;

import java.util.Stack;

public class Question3_5 {
	// While input stack is not empty, do the following
		// Pop an element from input stack and call it temp
		// While temp is smaller than top of temporary stack and temporary stack is not empty, pop from temporary stack and push into input stack
		// Push temp onto temporary stack
	// Temporary stack is the sorted stack
	
	static Stack<Integer> stack = new Stack<Integer>();
		
	static Stack sort() {
		Stack<Integer> secondary = new Stack<Integer>();

		while (!stack.isEmpty()) {
			int temp = stack.pop();

			while (!secondary.isEmpty() && temp < secondary.peek()) {
				int x = secondary.pop();
				stack.push(x);
			}
			secondary.push(temp);
		}
		
		while(!secondary.isEmpty()) {
			int x=secondary.pop();
			stack.push(x);
		}
		
		return stack;
	}
	
	public static void main(String args[]) {
		stack.push(34);
		stack.push(3);
		stack.push(31);
		stack.push(98);
		stack.push(92);
		stack.push(23);
		
		// Smallest on top
		Stack sorted = sort();
		
		System.out.print("Stack is: ");
		while(!sorted.isEmpty()) {
			System.out.print(sorted.pop() + " ");
		}
	} 

}
