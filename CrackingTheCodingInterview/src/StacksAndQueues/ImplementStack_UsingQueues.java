package StacksAndQueues;

import java.util.Queue;
import java.util.LinkedList;

public class ImplementStack_UsingQueues {

	static class Stack {
		Queue<Integer> q1=new LinkedList<Integer>();
		Queue<Integer> q2=new LinkedList<Integer>();
		
		void push(int data) {
			q1.add(data);
		}
		
		int pop() {
			int x=0;
			int sizeQ1=q1.size();
			
			while(q2.size()!=(sizeQ1-1)) {
				x=q1.poll();
				q2.add(x);
			}
			x=q1.poll();
			
			// Swap the queues
			Queue<Integer> q;
			q=q2;
			q2=q1;
			q1=q;
			
			return x;
		}
	}
	public static void main(String args[]) {
		Stack s = new Stack();
		s.push(1);
		s.push(2);
		s.push(3);		
		System.out.println(s.pop()); // This should give 3
		s.push(4);		
		System.out.println(s.pop()); // This should give 4
		System.out.println(s.pop()); // This should give 2
	}
}
