package StacksAndQueues;

public class Question3_2 {
	static class Stack{
		int MAX=1000;
		int[] arr = new int[MAX];
		int top=-1;
		int size=0;
	
	void push(int k, Stack min) {
		if(size>=MAX-1) {
			System.out.println("Overflow");
		}
		
		int minimum;
		if(min.size==0) {
			// Push k into the stack min
			min.top++;
			min.size++;
			min.arr[min.top]=k;		
			}
		else {
			minimum=min.arr[min.top];
			if(k<minimum) {
				// Push k into the stack min
				min.top++;
				min.size++;
				min.arr[min.top]=k;
			}			
		}
		top=top+1;
		size++;
		arr[top]=k;
	}
	
	int pop(Stack min) {
		if(size<=0) {
			System.out.println("Underflow");
		}		
		int x=arr[top];
		top=top-1;
		size--;
		int y=min.arr[min.top];
		
		if(x==y) {
			int out = min.arr[min.top];
			min.top--;
			min.size--;
			//System.out.println("Min updated");
		}
		return x;
	}
	
	int getMin(Stack min) {
		int minimum=min.arr[min.top];
		return minimum;
	}
	
	}
	
	public static void main(String args[]) {
		Stack s = new Stack();
		Stack min = new Stack();
		s.push(1,min);
		System.out.println("Pushed: 1");
		s.push(0,min);
		System.out.println("Pushed: 0");
		s.push(9,min);
		System.out.println("Pushed: 9");
		s.push(2,min);
		System.out.println("Pushed: 2");
		System.out.println("Min: "+s.getMin(min));		
		System.out.print("Popped: ");
		System.out.println(s.pop(min));
		System.out.print("Popped: ");
		System.out.println(s.pop(min));
		System.out.print("Popped: ");
		System.out.println(s.pop(min));
		System.out.println("Min: "+s.getMin(min));		
		s.push(-1,min);
		System.out.println("Pushed: -1");
		System.out.println("Min: "+s.getMin(min));	
		s.push(-2,min);
		System.out.println("Pushed: -2");
		System.out.println("Min: "+s.getMin(min));	
		System.out.print("Popped: ");
		System.out.println(s.pop(min));
		System.out.println("Min: "+s.getMin(min));	
	}	
}
