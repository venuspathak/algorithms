package StacksAndQueues;

import java.util.Stack;

public class BubbleSortUsingTwoStacks {

	static void printArray(int[] arr) {
		for(int i=0; i<arr.length; i++) {
			System.out.print(arr[i]+" ");
		}
	}
	
	static void sort(int arr[]) {
		Stack<Integer> s1=new Stack<Integer>();
		Stack<Integer> s2=new Stack<Integer>();
		
		for(int i=0; i<arr.length; i++) {
			s1.push(arr[i]);
		}
		
		while(!s1.isEmpty()) {
			int x=s1.pop();
			
			if(s2.isEmpty())
				s2.push(x);
			else {
				int top=s2.peek();
				int count=0;
				
				while(top<x && !s2.isEmpty()) {
					count++;
					s1.push(s2.pop());
					if(!s2.isEmpty())
						top=s2.peek();
				}
				s2.push(x);
				
				for(int i=0; i<count; i++) {
					s2.push(s1.pop());
				}
			}				
		}
		
		int j=0;
		while(!s2.isEmpty()) {
			arr[j]=s2.pop();
			j++;
		}		
	}
	
	public static void main(String args[]) {
		int[] arr = new int[] {15,12,44,2,5,10};
		System.out.print("Before sort: ");
		printArray(arr);
		sort(arr);
		System.out.println();
		System.out.print("After sort: ");
		printArray(arr);
	}
}
