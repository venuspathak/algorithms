package StacksAndQueues;

public class ImplementStack_Array {
	static class Stack {
		static int MAX=1000;
		static int[] arr = new int[MAX];
		static int size=0;
		static int top=-1;

		static boolean push(int n) {
			if(size>=MAX-1) {
				System.out.println("Stack Overflow");
				return false;
			}
			else {
				top=top+1;
				size++;
				arr[top] = n;
				return true;
			}			
		}

		static int pop() {
			if(size<=0) {
				System.out.println("Stack Underflow");
				return 0;
			}
			else {
				int x=arr[top];
				top=top-1;
				size--;
				return x;
			}
		}

		static boolean isEmpty() {
			if(size==0)
				return false;
			else 
				return true;
		}
		
		static void print() {
			if(size<=0)
				System.out.print("Stack is empty");
			for(int i=0; i<size; i++) {
				System.out.print(arr[i]+" ");
			}
		}
	}

	public static void main(String args[]) {
		Stack stack = new Stack();
		Stack.push(1);
		System.out.print("After pushing: ");		
		Stack.print();
		System.out.println();
		Stack.push(2);
		System.out.print("After pushing: ");		
		Stack.print();
		System.out.println();
		Stack.push(3);
		System.out.print("After pushing: ");		
		Stack.print();
		System.out.println();
		
		Stack.pop();
		System.out.print("After popping: ");		
		Stack.print();		
		System.out.println();

		boolean val1=Stack.isEmpty();
		if(val1==false)
			System.out.println("Stack is empty");
		else 
			System.out.println("Stack is not empty");
		
		Stack.pop();
		System.out.print("After popping: ");		
		Stack.print();		
		System.out.println();
		Stack.pop();
		System.out.print("After popping: ");		
		Stack.print();		
		System.out.println();
		
		boolean val2=Stack.isEmpty();
		if(val2==false)
			System.out.println("Stack is empty");
		else 
			System.out.println("Stack is not empty");
		
		Stack.pop();
		
		Stack.print();
	}
}
