package StacksAndQueues;

public class Implement_Stack_LinkedList {
	static Stack root;

	static class Stack {
		static int data;
		static Stack next;
		
		Stack(int data) {
			this.data=data;
			this.next=null;
		}
		
		static void push(int data) {
			Stack newNode = new Stack(data);
			if(root==null) {
				root=newNode;
			}
			else {
				Stack temp=root;
				root=newNode;
				newNode.next=temp;
			}
			System.out.println("Date pushed to stack: "+data);
		}
		
		static void pop() {
			if(root==null) {
				System.out.println("Stack is empty");
			}
			System.out.println("ROOT IS: "+root.next.data);
			root=root.next;
			System.out.println("ROOT IS: "+root.data);
		}
		
		static void isEmpty() {
			if(root==null) {
				System.out.println("Stack is empty");
			}
			else {
				System.out.println("Stack is not empty");
			}
		}
		
		static void print() {
			Stack current = root;
			
			while(current!=null) {
				System.out.print(current.data+" ");
				current=current.next;
			}
		}		
	}
	
	public static void main(String args[]) {
		Stack s = new Stack(1);
		Stack.push(2);
		Stack.push(3);
		Stack.push(4);
		/*Stack.print();

		Stack.push(5);
		
		Stack.pop();*/
		Stack.print();
	}
}
