package StacksAndQueues;

import java.util.Stack;
import java.util.Queue;
import java.util.LinkedList;

public class Stack_FindMiddleElement {
	static Stack<Integer> s1 = new Stack<Integer>();
	
	static int findMiddle() {
		int middleElement=0;
		Stack<Integer> s2=new Stack<Integer>();
		
		if(s1.isEmpty())
			return -1;
		
		int middleIndex=(0+(s1.size()-1))/2;
		System.out.println("Index of the middle element is: "+middleIndex);
		
		int start=0;	
		int compareValue=0;
		if(s1.size()%2==0) {
			compareValue=middleIndex+2;
		}
		else
			compareValue=middleIndex+1;
		
		while(start!=compareValue) {
			middleElement=(int) s1.pop();
			s2.push(middleElement);
			start++;
		}
		
		while(!s2.isEmpty()) {
			s1.push(s2.pop());
		}

		return middleElement;
	}
	
	public static void main(String args[]) {			
		s1.push(1);
		s1.push(2);
		s1.push(3);
		s1.push(4);
		s1.push(5);	
		System.out.println("Middle Element is: "+findMiddle());
		
		s1.push(6);
		System.out.println("Middle Element is: "+findMiddle());
		
		s1.push(7);
		System.out.println("Middle Element is: "+findMiddle());
	}
}
