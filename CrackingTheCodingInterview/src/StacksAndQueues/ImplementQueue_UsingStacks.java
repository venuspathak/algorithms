package StacksAndQueues;

import java.util.Stack;

public class ImplementQueue_UsingStacks {
	
	static class Queue{
		static Stack<Integer> s1=new Stack<Integer>();
		static Stack<Integer> s2=new Stack<Integer>();

		void enqueue(int k) {
			s1.push(k);
		}
		
		int dequeue() {
			while(s1.size()>1) {
				s2.push(s1.pop());
			}
			int x=s1.pop();
			
			while(!s2.isEmpty()) {
				s1.push(s2.pop());
			}			
			return x;
		}
	}
	
	public static void main(String args[]) {
		Queue q = new Queue();
		q.enqueue(1);
		q.enqueue(2);
		q.enqueue(3);
		q.enqueue(4);
		System.out.println(q.dequeue());
		q.enqueue(5);
		System.out.println(q.dequeue());
	}
}
