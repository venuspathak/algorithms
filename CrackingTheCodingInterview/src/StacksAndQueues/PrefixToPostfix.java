package StacksAndQueues;

import java.util.Stack; 
  
public class PrefixToPostfix { 
	
	static boolean isOperator(Character ch) {
		switch(ch) {
		case '+':
			System.out.println("Character "+ch+" is addition operator");
			return true;
		case '-':
			System.out.println("Character "+ch+" is subtraction operator");
			return true;
		case '*':
			System.out.println("Character "+ch+" is multiplication operator");
			return true;
		case '/':
			System.out.println("Character "+ch+" is division operator");
			return true;
		}
		return false;
	}
	
	static String convert(String str) {
		String result="";
		Stack<String> stack=new Stack<String>();
		
		str=new StringBuilder(str).reverse().toString();
		
		for(int i=0; i<str.length(); i++) {
			Character ch=str.charAt(i);
			
			if(isOperator(ch)) {
				String a=stack.pop();
				String b=stack.pop();
				
				String temp=a+b+ch;
				stack.push(temp);
			}
			else {
				stack.push(ch.toString());
			}
		}
		result=stack.pop();
			
		return result;
	}

	public static void main(String args[]) {
		String prefix="*-A/BC-/AKL";
		
		System.out.println("Prefix: "+prefix);
		System.out.println("Postfix: "+convert(prefix));
	}
} 
  
