package StacksAndQueues;

import java.util.Queue;
import java.util.LinkedList;
import java.util.Date;

class Animal {
	String type;
	Date arrived;
}

class Dog extends Animal {
	Dog() {
		super();
		type="dog";
	}
}

class Cat extends Animal {
	Cat() {
		super();
		type="cat";
	}
}

class AnimalShelter {
	Queue<Dog> dogs;
	Queue<Cat> cats;
	
	AnimalShelter() {
		dogs = new LinkedList<Dog>();
		cats = new LinkedList<Cat>();
	}
	
	void enqueue(Animal animal) throws Exception {
		animal.arrived=new Date();
		if(animal.type.equals("dog")) {
			dogs.add((Dog)animal);
		}
		else if(animal.type.equals("cat")) {
			cats.add((Cat)animal);
		}
		else {
			throw new Exception("Unknown type of animal");
		}
	}
	
	Animal dequeue() throws Exception {
		boolean dogEmpty=dogs.isEmpty();
		boolean catEmpty=cats.isEmpty();
		
		if(dogEmpty && catEmpty) {
			throw new Exception("There are no more animals");
		}
		
		if(dogEmpty) {
			return cats.poll();
		}
		
		if(catEmpty) {
			return dogs.poll();
		}
		else {
			Date dateDog=dogs.peek().arrived;
			Date dateCat=cats.peek().arrived;
			
			if(dateDog.compareTo(dateCat)<0) {
				// Dog is older
				return dogs.poll();
			}
			else {
				return cats.poll();
			}
		}
	}
	
	Dog dequeueDog() throws Exception {
		boolean dogEmpty=dogs.isEmpty();
		if(dogEmpty) {
			throw new Exception("There are no more dogs");
		}
		return dogs.poll();
	}
	
	Cat dequeueCat() throws Exception {
		boolean catEmpty=cats.isEmpty();
		if(catEmpty) {
			throw new Exception("There are no more cats");
		}
		return cats.poll();
	}
}

public class Question3_6 {
	
	public static void main(String args[]) throws Exception {
		AnimalShelter as = new AnimalShelter();
		as.enqueue(new Dog());
		as.enqueue(new Cat());
		as.enqueue(new Dog());
		as.enqueue(new Cat());
		as.enqueue(new Dog());
		as.enqueue(new Cat());
		as.enqueue(new Dog());
		as.enqueue(new Cat());
		as.enqueue(new Dog());
		as.enqueue(new Cat());
		
		System.out.println("Number of dogs: "+as.dogs.size());
		System.out.println("Number of cats: "+as.cats.size());

		as.dequeue();
		as.dequeueDog();
		as.dequeueCat();
		
		System.out.println("Number of dogs: "+as.dogs.size());
		System.out.println("Number of cats: "+as.cats.size());
	}
	
}
