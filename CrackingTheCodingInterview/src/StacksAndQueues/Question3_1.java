package StacksAndQueues;

public class Question3_1 {
	static class Stack {
		int MAX=1000;
		int top=-1;
		int size=0;
		int arr[] = new int[MAX];
		
		void push(int[] arr, int k) {
			if(size>=MAX-1) {
				System.out.println("Overflow");
			}
			
			top=top+1;
			size++;
			arr[top]=k;
		}
		
		void print(int arr[], int n, int size) {
			System.out.println("Stack "+n+" is");
			for(int i=0; i<size; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
		}
	}
	
	public static void main(String args[]) {	
		int source[] = new int[] {1,2,3,4,5,6,7};
		int size1=3;
		int size2=2;
		int size3=2;
		
		Stack s1 = new Stack();
		Stack s2= new Stack();
		Stack s3 = new Stack();
		
		for(int i=0; i<size1; i++) {
			//System.out.println("Stack 1: "+source[i]);
			s1.push(s1.arr, source[i]);
		}
		
		for(int i=size1; i<size1+size2; i++) {
			//System.out.println("Stack 2: "+source[i]);
			s2.push(s2.arr, source[i]);
		}
		
		for(int i=size1+size2; i<size1+size2+size3; i++) {
			//System.out.println("Stack 3: "+source[i]);
			s3.push(s3.arr, source[i]);
		}
		
		s1.print(s1.arr, 1, size1);
		s2.print(s2.arr, 2, size2);
		s3.print(s3.arr, 3, size3);
	}
}
