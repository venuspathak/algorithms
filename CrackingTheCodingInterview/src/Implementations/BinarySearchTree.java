package Implementations;

public class BinarySearchTree {
	Node root;
	static class Node{
		int key;
		Node left,right;
		
		Node(int val) {
			key=val;
			left=right=null;
		}
	}
	
	BinarySearchTree() {
		root=null;
	}
	
	BinarySearchTree(Node node) {
		root=node;
	}
	

	public static void main(String args[]) {
		BinarySearchTree tree = new BinarySearchTree();
		tree.root=new Node(10);
	}
	
}
