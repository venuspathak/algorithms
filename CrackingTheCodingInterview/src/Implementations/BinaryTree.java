package Implementations;

public class BinaryTree {
    Node root;
	 
	static class Node {
		int key;
		Node left;
		Node right;
		
		Node(int val) {
			key=val;
			left=null;
			right=null;
		}
	}
	
	BinaryTree() {
		root=null;
	}
	
	BinaryTree(Node node) {		
	root=node;	
	}
	
	static void traverseTree(Node root) {
		// Inorder Traversal: Left, Node, Right
		if (root == null)
			return;
		traverseTree(root.left);
		System.out.print(root.key + " ");
		traverseTree(root.right);

	}

	public static void main(String args[]) {
		BinaryTree tree = new BinaryTree();
		tree.root=new Node(1);
		tree.root.left=new Node(2);
		tree.root.right=new Node(3);
		tree.root.left.left=new Node(4);
		tree.root.left.right=new Node(5);
		tree.root.right.left=new Node(6);
		tree.root.right.right=new Node(7);
		
		traverseTree(tree.root);
	}
}
