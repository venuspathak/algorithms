package Implementations;

public class LinkedList {
	Node head;
	
	static class Node{
		int data;
		Node next;
		
		Node(int val) {
			data=val;
			next=null;
		}
	}
	
	public static LinkedList insertElement(LinkedList list, int val) {
		Node newNode= new Node(val);
		
		if(list.head==null) {
			list.head=newNode;
		}		
		else {
			Node current=list.head;

			while(current.next!=null) {
				current=current.next;
			}
			current.next=newNode;			
		}
		return list;
	}
	
	public static void printList(LinkedList list) {
		Node current = list.head;
		
		while(current!=null) {
			System.out.print(current.data+" ");
			current=current.next;
		}
		System.out.println();
	}
	
	public static void main(String args[]) {
		LinkedList list = new LinkedList();
		
		list = insertElement(list,1);
		list = insertElement(list,2);
		list = insertElement(list,3);
		list = insertElement(list,4);
		list = insertElement(list,5);
		list = insertElement(list,6);
		list = insertElement(list,7);
		
		printList(list);
	}
}
