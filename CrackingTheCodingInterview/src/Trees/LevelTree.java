package Trees;

public class LevelTree {
	Node root;
	
	static class Node {
		int data;
		Node left;
		Node right;
		
		Node(int data) {
			this.data=data;
		}
	}
	
	LevelTree() {
		root=null;
	}
	

	static int findLevel(Node root, int k, int level) {
		int levelMethod=0;
		
		if(root==null)
			return 0;
		else {
			if(root.data==k) {
				return level;
			}
			else {
				levelMethod=findLevel(root.left, k, level+1);
				if(levelMethod!=0)
					return levelMethod;
				levelMethod=findLevel(root.right, k, level+1);
			}
		}
		
		return levelMethod;
	}
	
	
	public static void main(String args[]) {
		LevelTree tree = new LevelTree();
		tree.root=new Node(1);
		tree.root.left=new Node(2);
		tree.root.right=new Node(3);
		tree.root.left.left=new Node(4);
		tree.root.left.right=new Node(5);
		
		int result=findLevel(tree.root, 4, 0);
		System.out.println("Level of the tree is: "+result);
	}
}
