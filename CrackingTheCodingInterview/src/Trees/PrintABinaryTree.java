package Trees;

public class PrintABinaryTree {
	Node root;
	
	static class Node {
		int data;
		Node left;
		Node right;
		
		Node(int data) {
			this.data=data;
			this.left=null;
			this.right=null;
		}
	}
	
	PrintABinaryTree() {
		root=null;
	}
	
	PrintABinaryTree(int data) {
		root=new Node(data);
	}
	
	static void print(Node root, int i) {
		if(root==null)
			return;
		
		else {
			if(i==0) {
				System.out.print(root.data+" ");
			}
			if(root.left!=null) {
				i++;
				Node left=root.left;
				System.out.print(left.data+" ");				
				print(left,i);
			}
			
			if(root.right!=null) {
				i++;
				Node right=root.right;				
				System.out.print(right.data+" ");				
				print(right,i);
			}
		}
	}
	
	public static void main(String args[]) {
		PrintABinaryTree bt = new PrintABinaryTree();
		bt.root=new Node(1);
		bt.root.left=new Node(2);
		bt.root.right=new Node(3);
		bt.root.left.left=new Node(4);
		bt.root.left.right=new Node(5);
		bt.root.right.left=new Node(6);
		bt.root.right.right=new Node(7);
		bt.root.left.left.left=new Node(8);
		
		int i=0;
		
		print(bt.root, i);
	}
}
