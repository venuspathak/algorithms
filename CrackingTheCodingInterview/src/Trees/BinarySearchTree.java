package Trees;

class Node {
	int data;
	Node left;
	Node right;
	
	Node(int data) {
		this.data=data;
		this.left=null;
		this.right=null;
	}
}

public class BinarySearchTree {
	Node root;
	
	BinarySearchTree() {
		root=null;
	}
	
	static Node insert(Node root, int data) {
		if(root==null)  {
			root=new Node(data);
			return root;
		}
		if(data<root.data)
			root.left=insert(root.left, data);
		else if(data>root.data)
			root.right=insert(root.right, data);
		
		return root;
	}
	
	static Node delete(Node root, int data) {
		if(root==null)
			return null;
		if(data<root.data) {
			root.left=delete(root.left,data);
		}
		else if(data>root.data) {
			root.right=delete(root.right,data);
		}
		else {
			// This is the node that needs to be deleted
			
			// If no or one child
			if(root.right==null)
				return root.left;
			else if(root.left==null)
				return root.right;
			else {
				// If two children
				root.data=minValue(root.right);
				root.right=delete(root.right,root.data);
			}			
		}
		return root;
	}
	
	static int minValue(Node root) {
		int min=root.data;
		if(root.left!=null) {
			min=root.left.data;
			root=root.left;
		}
		return min;
	}
 	
	static void inorder(Node root) {
		if(root!=null) {
			inorder(root.left);
			System.out.print(root.data+" ");
			inorder(root.right);
		}
	}
	
	static void preorder(Node root) {
		if(root!=null) {
			System.out.print(root.data+" ");
			preorder(root.left);
			preorder(root.right);
		}
	}
	
	static void postorder(Node root) {
		if(root!=null) {
			postorder(root.left);
			postorder(root.right);
			System.out.print(root.data+" ");
		}
	}

	public static void main(String args[]) {
		BinarySearchTree bst = new BinarySearchTree();
		bst.root=insert(bst.root, 50);
		bst.root=insert(bst.root, 30);
		bst.root=insert(bst.root, 20);
		bst.root=insert(bst.root, 40);	
		bst.root=insert(bst.root, 70);
		bst.root=insert(bst.root, 60);
		bst.root=insert(bst.root, 80);
		
		inorder(bst.root);
		System.out.println();
		preorder(bst.root);
		System.out.println();
		postorder(bst.root);
		System.out.println();
		
		System.out.println("DELETIONS");
		
		bst.root=delete(bst.root,20);
		inorder(bst.root);
		System.out.println();
		bst.root=delete(bst.root,30);
		inorder(bst.root);
		System.out.println();
		bst.root=delete(bst.root,50);
		inorder(bst.root);
		System.out.println();
	}
}
