package Trees;

public class BreadthFirstSearchOrLevelOrder {
	Node root;
	
	static class Node {
		int data;
		Node left;
		Node right;
		
		Node(int data) {
			this.data=data;
		}
	}
	
	BreadthFirstSearchOrLevelOrder() {
		root=null;
	}
	
	static void print(Node root) {
		// Find height
		int h=findHeight(root);
		System.out.println("Height of the tree is: "+h);
		
		for(int i=1; i<=h; i++)
			printLevelOrder(root,i);		
	}
	
	static int findHeight(Node root) {
		if(root==null)
			return 0;
		else {
			int lHeight=findHeight(root.left);
			int rHeight=findHeight(root.right);
			
			if(lHeight>rHeight)
				return lHeight+1;
			else
				return rHeight+1;
		}
	}
	
	static void printLevelOrder(Node root, int level) {
		if(root==null)
			return;
		if(level==1)
			System.out.print(root.data+" ");
		else if(level>1) {
			printLevelOrder(root.left,level-1);
			printLevelOrder(root.right,level-1);
		}
	}
	
	
	public static void main(String args[]) {
		BreadthFirstSearchOrLevelOrder bt = new BreadthFirstSearchOrLevelOrder();
		bt.root=new Node(1);
		bt.root.left=new Node(2);
		bt.root.right=new Node(3);
		bt.root.left.left=new Node(4);
		bt.root.left.right=new Node(5);
		
		print(bt.root);
	}
}
