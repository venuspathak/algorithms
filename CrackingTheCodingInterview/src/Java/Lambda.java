package Java;

interface Test {
	void testMethod(int x);
}

public class Lambda {

	public static void main(String args[]) {
		Test obj = (int x)->System.out.println(2*x);
		obj.testMethod(5);
	}
}
