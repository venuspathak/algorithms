package Java;

import java.util.List;
import java.util.ArrayList;

public class Country {
	private String countryName="";
	private String continent="";
	private int population;
	
	Country(String countryName, String continent) {
		this.countryName=countryName;
		this.continent=continent;
		this.population=0;
	}

	static String getContinent(Country country) {
		return country.continent;
	}
	
	static void setPopulation(Country country, int p) {
		country.population=p;
	}
	
	static int getPopulation(List<Country> countries, String continent) {
		// Computes total population of the continent
		
		int p=0;
		
		for(Country country:countries) {
			p=p+country.population;
		}
		
		return p;
	}
	
	public static void main(String args[]) {
		Country country1 = new Country("India","Asia");
		Country country2 = new Country("Pakistan","Asia");
		
		setPopulation(country1,2000);
		setPopulation(country2,1000);		

		List<Country> list=new ArrayList<Country>();
		list.add(country1);
		list.add(country2);
		
		int result=getPopulation(list,"Asia");
		System.out.println("Population of the continent is: "+result);
	}
}
