package Java;

import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

class ClassToInspect {
	
	ClassToInspect() {
		System.out.println("This is the constructor");
	}
	
	public void method1() {
		System.out.println("This is method1");
	}
	
	public void method2() {
		System.out.println("This is method2");
	}
	
	private void method3() {
		System.out.println("This is method3");
	}	
}

public class ReflectionAPI {

	public static void main(String args[]) throws NoSuchMethodException, SecurityException {
		ClassToInspect obj = new ClassToInspect();
		
		Class className=obj.getClass();
		System.out.println("PRINTING CLASS NAME");
		System.out.println(className.getName());
		
		System.out.println("PRINTING METHOD NAMES");
		for(Method method:className.getMethods()) {
			System.out.println(method.getName());
		}
		
		System.out.println("PRINTING CONSTRUCTOR NAMES");
		Constructor[] constructors=className.getConstructors();
		System.out.println(constructors.length);
	}
}
