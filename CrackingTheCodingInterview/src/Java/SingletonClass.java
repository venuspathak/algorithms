package Java;

class Singleton {
	static Singleton obj=null;
	static int x=10;
	
	private Singleton() {
	}
	
	public static Singleton getInstance() {
		if(obj==null)
			obj=new Singleton();
		return obj;
	}
}

public class SingletonClass {
	public static void main(String args[]) {
		Singleton a=Singleton.getInstance();
		Singleton b=Singleton.getInstance();
		
		a.x=a.x+10;
		
		System.out.println("A's x is: "+a.x);
		System.out.println("B's x is: "+b.x);
	}
}
