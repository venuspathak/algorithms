package Java;

public class ConstructorChainingSameClass {
	
	ConstructorChainingSameClass() {
		this(5);
		System.out.println("This is default constructor");
	}
	
	ConstructorChainingSameClass(int x) {
		this(5,10);
		System.out.println("One argument constructor: "+x);
	}
	
	ConstructorChainingSameClass(int x, int y) {
		System.out.println("Two arguments constructor: "+x+","+y);
	}
	
	public static void main(String args[]) {
		ConstructorChainingSameClass obj = new ConstructorChainingSameClass();
	}
}
