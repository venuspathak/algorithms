package Java;

public class ReturnInTry {

	
	static int returnIntTryBlock() {
		try {
			return 10;
		}
		finally {
			System.out.println("Inside finally block");
		}
	}
	
	static int returnIntTryBlock2() {
		try {
			return 10;
		}
		finally {
			System.out.println("Inside finally block 2");
			return 20;
		}
	}
	
	public static void main(String args[]) {
		System.out.println(returnIntTryBlock());
		System.out.println(returnIntTryBlock2());
	}
}
