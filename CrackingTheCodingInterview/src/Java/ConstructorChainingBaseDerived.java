package Java;

class Base {
	
	Base() {
		this(5);
		System.out.println("Default constructor of base class");
	}
	
	Base(int x) {
		System.out.println("Single parameter constructor of base class: "+x);
	}
	
	Base(int x, int y) {
		System.out.println("Double parameters constructor of base class: "+x+","+y);
	}
} 

public class ConstructorChainingBaseDerived extends Base {
	
	ConstructorChainingBaseDerived() {
		super();
		System.out.println("Default constructor of derived class");
	}
	
	ConstructorChainingBaseDerived(int x) {
		super(x,10);
		System.out.println("Single parameter constructor of derived class");
	}
	
	public static void main(String args[]) {
		ConstructorChainingBaseDerived obj = new ConstructorChainingBaseDerived();
		System.out.println("******************************************************");
		ConstructorChainingBaseDerived obj2 = new ConstructorChainingBaseDerived(5);
	}
}

