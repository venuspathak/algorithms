package Misc;

public class LCM {
	static int m=5, n=12, lcmNumbers;
	
	static void findLCM(int m, int n) {
		lcmNumbers = (m>n)?m:n;
		
		while(true) {
			if(lcmNumbers%m==0 && lcmNumbers%n==0) {
				System.out.println("LCM is: "+lcmNumbers);
				break;
			}
			
			lcmNumbers++;
		}
	}
	
	public static void main(String args[]) {
		// LCM is Least Common Multiplier
		// LCM of 5 and 10 would be 10
		// 5=5, 10=5*2, LCM=5*2
		// LCM of 5 and 7 would be 35
		// 5=5, 7=5, LCM=7*5
		
		findLCM(m, n);
	}
}
