package Misc;

public class TotalGuests {
	static int n=5;
	static int[] arr=new int[] {1,0,2,1,3};
	
	static int findGuests() {
		int guests=0;
		
		for(int i=0; i<arr.length; i++) {
			int guestNeed=arr[i];
			
			if(guests<guestNeed) {
				// This guest leaves
			}
			else {
				// This guest stays
				guests++;
			}
		}
		return guests;
	}
	
	public static void main(String args[]) {
		int result=findGuests();
		System.out.println("Total number of guests: "+result);
	}
}
