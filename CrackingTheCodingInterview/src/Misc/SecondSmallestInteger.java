package Misc;

public class SecondSmallestInteger {

	static void findMin(int[] arr) {
		int min=arr[0];
		int second=arr[0];
		
		for(int i=0; i<arr.length; i++) {
			if(arr[i]>min) {
				// Do not update min
				if(second>arr[i]) {
					second=arr[i];
				}
			}
			else {
				// Update min and second min
				second=min;
				min=arr[i];
			}
		}
		System.out.println("Minimum is: "+min);
		System.out.println("Second minimum is: "+second);
	}
	
	public static void main(String args[]) {
		int arr[]= new int[] {2,3,0,6,-1};
		
		findMin(arr);
	}
}
