package Misc;

import java.util.regex.Pattern;

public class CheckEmail {	
	
	static boolean checkEmail(String email) {
		String regexPattern = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                "[a-zA-Z0-9_+&*-]+)*@" + 
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                "A-Z]{2,7}$"; 
		
		Pattern pattern = Pattern.compile(regexPattern);
		boolean returnVal=pattern.matcher(email).matches();
		return returnVal;
	}
	
	public static void main(String args[]) {
		String email="pathak.venus0706@yahoo..com";
		Boolean val=checkEmail(email);
		System.out.println(val);
	}
}
