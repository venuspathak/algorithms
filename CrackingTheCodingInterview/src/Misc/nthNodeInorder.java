package Misc;

class Node21 {
	int data;
	Node21 left;
	Node21 right;
	
	Node21(int data) {
		this.data = data;
		this.left = null;
		this.right = null;
	}
}

public class nthNodeInorder {
	static int counter=0;
	
	static void nthNode(Node21 root, int n) {
		Node21 current=root;
	
		if(current==null)
			return;		
	
		nthNode(current.left,n);
		counter++;
		if(counter==n)
			System.out.println(current.data);
		
		nthNode(current.right,n);
		
	}
	
	public static void main(String args[]) {
		Node21 root=new Node21(1);
		root.left=new Node21(2);
		root.right=new Node21(3);
		root.left.left=new Node21(4);
		root.left.right=new Node21(5);
		root.right.left=new Node21(6);
		root.right.right=new Node21(7);
		
		int n=6;
		
		nthNode(root,n);
	}
}
