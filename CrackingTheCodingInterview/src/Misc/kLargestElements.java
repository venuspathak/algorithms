package Misc;

import java.util.ArrayList;
import java.util.List;

public class kLargestElements {

	
	static void sort(int[] arr, int k) {
		int len = arr.length;
		List<Integer> list=new ArrayList<Integer>();
		
		for(int i=0; i<k; i++) {
			for(int j=i; j<len-i-1; j++) {
				if(arr[j]>arr[j+1]) {
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
				if(j==len-i-2)
					list.add(arr[j+1]);
			}
		}
		
		for(int i=0; i<list.size(); i++) {
			System.out.print(list.get(i)+" ");
		}
	}
	
	
	public static void main(String args[]) {
		int arr[]=new int[]{1, 23, 12, 9, 30, 2, 50};
		int k=3;
		
		sort(arr,k);
	}
}
