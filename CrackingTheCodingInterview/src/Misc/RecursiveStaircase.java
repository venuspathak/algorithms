package Misc;

public class RecursiveStaircase {
	
	static int fib(int n) {
		if(n<=1)
			return n;
		else {
			return fib(n-1) + fib(n-2); 
		}
	}
	
	static int count(int n) {
		return fib(n+1);
	}
	
	public static void main(String args[]) {
		int n=4;
		
		System.out.println(count(n));
	}
}
