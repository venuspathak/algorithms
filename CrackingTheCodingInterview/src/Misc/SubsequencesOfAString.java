package Misc;

import java.util.HashSet;

public class SubsequencesOfAString {
	static HashSet<String> set=new HashSet<String>();

	static void subsequence(String str) {
		
		for(int i=0; i<str.length(); i++) {
			for(int j=str.length(); j>i; j--) {
				String subString=str.substring(i,j);
				
				if(!set.contains(subString))
					set.add(subString);
				
				for(int k=1; k<subString.length()-1; k++) {
					StringBuffer sb = new StringBuffer(subString);
					
					sb.deleteCharAt(k);
					subsequence(sb.toString());
				}				
			}
		}
	}
	
	public static void main(String args[]) {
		String str="abc";
		
		subsequence(str);
		System.out.println(set);
	}
}
