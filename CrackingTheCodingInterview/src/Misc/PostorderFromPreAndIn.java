package Misc;

public class PostorderFromPreAndIn {
	static int preIndex=0;

	
	static int search(int[] in, int root, int start, int end) {
		for(int i=start; i<=end; i++) {
			if(root==in[i])
				return i;
		}
		return -1;
	}
	
	static void printPostorder(int[] in, int[] pre, int start, int end) {
		
		if(start<=end) {
			int index=search(in, pre[preIndex], start, end);
			preIndex=preIndex+1;
			
			if(index!=-1) {
				printPostorder(in, pre, start, index-1);
				printPostorder(in, pre, index+1, end);
				System.out.print(in[index]+" ");
			}
		}		
	}
	
	public static void main(String args[]) {
		int in[] = new int[] {4,2,5,1,3,6};
		int pre[] = new int[] {1,2,4,5,3,6};
		int len=in.length;
		
		printPostorder(in,pre,0,len-1);
	}
}
