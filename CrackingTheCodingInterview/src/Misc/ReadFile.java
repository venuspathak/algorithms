package Misc;

import java.io.*;

public class ReadFile {

	public static void main(String args[]) throws Exception {
		
		File file = new File("C:\\setup.log");
		BufferedReader br = new BufferedReader(new FileReader(file));
		
		String st;
		while((st=br.readLine())!=null) {
			System.out.println(st);
		}
		
		/*
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
		String st2 = br2.readLine();
		System.out.println(st2);
		*/
	}
}
