package Misc;

public class MultiplicationTable {
	
	static void printTables() {
		for(int i=1; i<=12; i++) {
			for(int j=1; j<=10; j++) {
				System.out.print(String.format("%4d", i*j));
			}
			System.out.println();
		}
	}
	
	public static void main(String args[]) {
		printTables();
	}
}
