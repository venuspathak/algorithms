package Misc;

public class kthSmallestInteger {
	
	static int partition(int[] arr, int low, int high) {
		int i=low-1;
		int pivot=arr[high];
		
		for(int j=low; j<high; j++) {
			if(arr[j]<pivot) {
				i++;
				
				int temp=arr[j];
				arr[j]=arr[i];
				arr[i]=temp;
			}
		}
		int temp=arr[high];
		arr[high]=arr[i+1];
		arr[i+1]=temp;
		
		return i+1;
	}
	
	static int findKth(int arr[], int k, int low, int high) {		
		if(k>0 && k<=(high-low+1)) {
			int partition=partition(arr,low,high);
			
			if(partition-low==k-1)
				return arr[partition];
			else if(partition-low>k-1) {
				// Find in left subarray
				return findKth(arr,k,low,partition-1);
			}
			else {
				return findKth(arr,k-partition+low-1,partition+1,high);
			}			
		}
		return -1;
	}	
	
	public static void main(String args[]) {
		int arr[] = new int[] {12,3,5,7,4,19,26};
		int low=0;
		int high=arr.length-1;
		int k=3;
		
		System.out.println(k+"th smallest element is: "+findKth(arr,k,low,high));
	}
}
