package Misc;

public class GCD {
	static int m=55, n=121, gcdNumbers;
	
	static void findGCD(int m, int n) {
		gcdNumbers=Math.min(m, n);
		
		while(true) {
			if(m%gcdNumbers==0 && n%gcdNumbers==0) {
				System.out.println("GCD is: "+gcdNumbers);
				break;
			}			
			gcdNumbers--;
		}
	}

	public static void main(String args[]) {
		// GCD of 6 and 24 is 2
		findGCD(m,n);
	}
}
