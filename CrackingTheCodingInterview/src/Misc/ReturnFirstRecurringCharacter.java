package Misc;
import java.util.HashSet;

public class ReturnFirstRecurringCharacter {
	
	static void recurring(String str) {
		Character ch;
		HashSet<Character> map=new HashSet<Character>();
		
		for(int i=0; i<str.length(); i++) {
			Character chStr=str.charAt(i);
			
			if(map.contains(chStr)) {
				System.out.println("First recurring character is: "+chStr);
				break;
			} else {
				map.add(chStr);
			}
		}
	}
	
	public static void main(String args[]) {
		String str="BCADE";
		recurring(str);		
	}
}
