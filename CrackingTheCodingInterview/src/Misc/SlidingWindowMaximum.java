package Misc;

import java.util.Deque;
import java.util.LinkedList;

class SlidingWindowMaximum {
	
	static void findMax(int[] arr, int k) {
		Deque<Integer> deq=new LinkedList<Integer>();
		
		// Run the loop for k elements
		for(int i=0; i<k; i++) {
			while(!deq.isEmpty() && arr[i]>=arr[deq.peekLast()])
				deq.removeLast();
			deq.addLast(i);
		}
		
		// Run loop from k until (length-1)th position
		for(int i=k; i<arr.length; i++) {
			// Print the front element of the deque
			System.out.print(arr[deq.peek()]+" ");
			
			// Remove front if window exceeds
			while(!deq.isEmpty() && deq.peek()<=i-k) {
				deq.removeFirst();
			}
			
			// Remove last until current element is greater than the last element
			while(!deq.isEmpty() && arr[i]>=arr[deq.peekLast()]) {
				deq.removeLast();
			}
			deq.addLast(i);
		}
		System.out.print(arr[deq.peek()]+" ");
	}
	 
	public static void main(String args[]) {
		int arr[] = new int[] {12, 1, 78, 90, 57, 89, 56};
		int k=3;
		
		findMax(arr,k);
	}	
}
