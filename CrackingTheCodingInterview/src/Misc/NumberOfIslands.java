package Misc;

public class NumberOfIslands {
	
	static Boolean isSafe(int i, int j, int ROW, int COL, Boolean[][] visited, int[][] matrix) {
		return (i>=0) && (j>=0) && (i<ROW) && (j<COL) && (!visited[i][j]) && (matrix[i][j]==1);
	}
	
	static void DFSTraversal(int[][] matrix, Boolean[][] visited, int i, int j, int ROW, int COL) {
		visited[i][j]=true;
		
		int rows[]= new int[]{-1,0,-1,0,0,0,1,1,1};
		int cols[]= new int[]{-1,0,1,-1,0,1,-1,0,1};
		
		for(int count=0; count<8; i=count++) {
			if(isSafe(i+rows[count],j+cols[count],ROW,COL,visited,matrix))
				DFSTraversal(matrix,visited,i+rows[count],j+cols[count],ROW,COL);
		}
	}
	
	static int numberOfIslands(int[][] matrix, int ROW, int COL) {
		int number=0;
		Boolean visited[][]=new Boolean[ROW][COL];
		
		for(int i=0; i<ROW; i++) {
			for(int j=0; j<COL; j++) {
				visited[i][j]=false;
			}
		}
		
		for(int i=0; i<ROW; i++) {
			for(int j=0; j<COL; j++) {
				if(!visited[i][j] && matrix[i][j]==1) {
					DFSTraversal(matrix,visited,i,j, ROW, COL);
					number++;
				}
			}
		}
		
		return number;
	}

	public static void main(String args[]) {
		int matrix[][]= {{1,1,0,0,0},{0,1,0,0,1},{1,0,0,1,1},{0,0,0,0,0},{1,0,1,0,1}};
		int ROW=5;
		int COL=5;
		
		int result=numberOfIslands(matrix,ROW,COL);
		
		System.out.println("Number of islands: "+result);
	}
}
