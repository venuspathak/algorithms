package Misc;

import java.util.Stack;

public class StockSpanProblem {
	
	static void calculateSpan(int[] priceArr, int len, int[] span) {
		Stack<Integer> st=new Stack<Integer>();
		st.push(0);
		span[0]=1;
		
		for(int i=1; i<len; i++) {
			while(!st.isEmpty() && priceArr[st.peek()]<=priceArr[i])
				st.pop();
			
			if(st.isEmpty()) {
				span[i]=i+1;
			}
			else {
				span[i]=i-st.peek();
			}
			
			st.push(i);
		}
	}
	
	static void printArray(int[] arr) {
		for(int i=0; i<arr.length; i++)
			System.out.print(arr[i]+" ");
	}

	public static void main(String args[]) {
		int[] priceArr=new int[] {10,4,5,90,120,80};
		int len=priceArr.length;		
		int[] span=new int[len];
		
		calculateSpan(priceArr,len,span);
		printArray(span);
	}
}
